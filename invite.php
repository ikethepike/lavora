
<?php include 'inc/head.php'; ?>

<?php 
	
	$lost = true;

	if(isset($_GET['id'])){
		$projectHash = $_GET['id'];
		$lost = false;


		$DB = DB::getInstance();

		$project = $DB->get('projects', array('hash', '=', $projectHash))->first();


		$user = new User();

		if($user->isLoggedIn()){

			$currentProjects 	= (array) json_decode($user->data()->projects);

			if(!in_array($project->id, $currentProjects)){
				$currentProjects[] 	= $project->id;

				$DB->update('users', array('id', $user->data()->id), array(
						'projects' => json_encode($currentProjects),
				));
			}

			Redirect::to(HOME_URL . '?id=' . $project->id);

		}




	}

?>


</head>
<body class='invite-page'>

<?php if(!$lost): ?>
	

<div class="blur" style='background-image: url(<?php echo $project->feat_img; ?>);'></div>
		
<?php endif; ?>


<div id="invite-popup">
	<header>
		<h1>
			<?php echo ($lost) ? "Looks like you are lost buddy" :  "You're invited to  " . $project->name; ?>  
		</h1>

		<p>Do-Do is designed to help you manage your tasks and projects</p>
	</header>

	<div class="button-wrapper">
		<a href="<?php echo (!$lost) ? HOME_URL . '?invite=' . $projectHash : HOME_URL ; ?> "> Login </a>

		<a href="<?php echo (!$lost) ? HOME_URL . '/register?invite=' . $projectHash : HOME_URL . "/register"; ?> "> Register </a>

	</div>
</div>



</body>
</html>