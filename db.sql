-- MySQL dump 10.16  Distrib 10.1.13-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: lavora
-- ------------------------------------------------------
-- Server version	10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `applications`
--

DROP TABLE IF EXISTS `applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `install_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `device` varchar(255) COLLATE utf8_bin NOT NULL,
  `token` varchar(72) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applications`
--

LOCK TABLES `applications` WRITE;
/*!40000 ALTER TABLE `applications` DISABLE KEYS */;
INSERT INTO `applications` VALUES (1,'','2016-08-17 11:15:35','','cc605f8965b23d387aae8011d01d4c2d8df23c06c97865df4d3ccd34e8228642ecbee987',0),(2,'','2016-08-17 11:20:06','','c64575f7ac4abee79952c8c790d09a87cb6c41c444517bf9aba766c80b55a2db4bce0997',0),(3,'','2016-08-17 11:44:33','','62f66972c88e363e89f5e6e1d1c85ddabde52ff9198a9c617989747a2ba37f71f943de94',0),(4,'','2016-08-17 11:44:35','','d9e04713c20cf9dd8e3deadafaf7d845dd39fdf4388ac812e0fc97e8f949c21083d395e4',0),(5,'','2016-08-17 11:52:30','','cfe4e91f0074bd0e3d98e31d4a551bb32f614c5ad63f8cb46ba841ade2d596b3471ecf49',0);
/*!40000 ALTER TABLE `applications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `category` varchar(100) NOT NULL,
  `owners` varchar(1024) NOT NULL,
  `tasks` varchar(60000) NOT NULL,
  `budget` int(11) NOT NULL,
  `start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `etc` date NOT NULL,
  `hash` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `feat_img` varchar(255) NOT NULL,
  `currency` varchar(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (12,'Colorscapes','Testing new projects','Design','[\"6\", \"47\"]','',50,'2016-06-09 13:58:23','2016-01-01','asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf',0,'uploads/678eedaa74fe2dda4bfccb6eb1629627a591b3c8.jpg','$'),(15,'The weather is cool ','The weather is nice today','Design','[\"6\"]','',1250,'2016-06-11 12:36:02','2017-05-10','44d7fa9e9d07825f39d18f71c0d9b2117b8748026e9971d0e613d6f924618aec240df9ccc54203cdc98172e3ca33a16769adb1b5cea7bf3cb59c4cf2d0cd',0,'uploads/3f0256944ba32f3e01e92b67d54807402443420b.jpg',''),(27,'Do-Do','Do-do is a task manager and CRM solution that aims to take the pain out of managing teams and communicating with clients. ','Development','[\"6\",\"47\"]','',0,'2016-08-08 08:52:56','2016-09-09','ea55f11ab705fc9ff1e9bd2aea272f806845e354b2c04aebfdd7d8874ab5e2da',0,'uploads/34a51633c7a528c366bf.jpg',''),(28,'Test project','A test project','Design','[\"6\"]','',9000000,'2016-08-08 13:29:57','2019-02-04','84bf8df5d8669b22b7b14660ee37bd33fe1d7bebea1dff279a5fcb597471daa9',0,'uploads/8e62ca8118665c1f05d7.jpg',''),(29,'Testy test','hey','Development','[\"6\"]','',18200,'2016-08-08 13:37:09','2019-03-02','4660c53aa382e2b39763221a7a5c784d4ad8f05a022497fc5cdaa471545b6862',0,'','GBP');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `description` text NOT NULL,
  `owner` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `due` date NOT NULL,
  `priority` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `hash` varchar(77) NOT NULL,
  `last_revision` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `revisions` mediumtext NOT NULL,
  `feat_img` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES (42,'First task',0,'This is the first task',6,14,'2016-06-12 13:53:27','2016-01-01',0,'',6,'7bf08858d5f0db40cf7420b5148a19a0f5269909b8c640bd0f6d137b0b5b663ad5a682bd6563','0000-00-00 00:00:00','',''),(43,'First task',0,'There have been thousands of books written by fathers about how wonderful their children are. And there have been many books written by children about how wonderful their fathers are. And all those things are good. I think my kids are the greatest kids in the world, too, just like most parents do. But I didn\'t think we needed another book like that. I wanted to step around all those books and say, \"yes we all think we know a lot about fathers and what they do for their kids, but what do we really know based on scientific research?\" So what I\'ve tried to do here is collect a lot of the new and recent research that says, \"Here\'s what fathers really do for their children.\" And once we know that, then we can talk about all the things we want to say about fathers and social policies and political issues and everything, on a basis of what we really know, not on a basis of how we think fathers behave.',6,14,'2016-06-12 13:53:30','2016-01-01',0,'',6,'92926a54976bcd97c0bc77ea7f8a269f52ce307c764fd2a3b87f9e6a79e2f6a4500c4e55f1ce','0000-00-00 00:00:00','','uploads/3f0256944ba32f3e01e92b67d54807402443420b.jpg'),(44,'Design UI overlays',0,'Because they would look good, in maybe like a spicy red. ',6,15,'2016-07-05 07:37:59','2022-01-01',0,'',6,'f5d9894b6cb1b62b6a98bf91ea87d37b0dc787a7b8f8ec5ab5ae4b53789b4012185f6f364b5a','0000-00-00 00:00:00','',''),(45,'Create UI Mocks',3,'The landing page needs to be redesigned',47,12,'2016-07-24 16:45:51','2016-09-01',0,'',6,'695b50080104000863254b2584be2b23145e62c9e4d2e0ceeca9822a4ae19953077bcd0bafd71','0000-00-00 00:00:00','','uploads/c3ed45eda7e795884b2eb7fc25aff4daaa33c991.jpg'),(46,'Dogcar',3,'Enable password hashing using BCRYPT for all users',6,12,'2016-07-24 16:50:49','2018-12-02',0,'',6,'84d75a9cf5fc6c891ca29ccd02fd79586a8eec952ba8600f71e86fe53dab4b17136cf3c7dda4','0000-00-00 00:00:00','','uploads/2f70921cec3dde0bebb81967b8024a8549477b27.png'),(47,'Password Hashing',2,'Enable password hashing using BCRYPT for all users',6,12,'2016-07-24 16:51:06','2018-12-02',0,'',6,'959a3b40f57948521ad8ee74b6eb72ca1dde938cd0a00afce6d71fa6db8e05a96e963ec3a43e','0000-00-00 00:00:00','','uploads/2f70921cec3dde0bebb81967b8024a8549477b27.png'),(48,'Fix up team overview',1,'The team overview page is super duper broken and needs fixing really badly. ',6,12,'2016-08-06 15:13:28','2016-01-02',0,'',6,'ef827dc2bba65024eb88943d7c22e3393d94e25e2825a134277196b8c8164c','0000-00-00 00:00:00','','uploads/561e994354366a127e43.png'),(50,'Add the delete functionion',2,'Projects are undeletable',47,12,'2016-08-06 15:19:16','2016-01-01',0,'',6,'fc3b6fa0484ef137f501e28a3f605fe0d785f63e32a03311e3623e7a5844af','0000-00-00 00:00:00','','uploads/a84aa6f67e302e0a89f7.jpg'),(51,'Add the delete functionion',1,'Projects are undeletable',47,12,'2016-08-06 15:19:46','2016-01-01',0,'',6,'6f20980c04b89937002d9ceadc7e20969aa3815945dc6bad69efdd402dc9ed','0000-00-00 00:00:00','','uploads/a84aa6f67e302e0a89f7.jpg'),(52,'Add the delete functionion',2,'Projects are undeletable',47,12,'2016-08-06 15:19:55','2016-01-01',0,'',6,'9e441efdfbc902fe99b2c35ae7782832271a2ec94113384938fe13c9eee45b','0000-00-00 00:00:00','','uploads/a84aa6f67e302e0a89f7.jpg'),(53,'Add the delete functionion',1,'Projects are undeletable',47,12,'2016-08-06 15:19:55','2016-01-01',0,'',6,'dd75e175fcf7274ca8e7651aad19bc66e8fe9b80092b0af39b0cc4308dbca4','0000-00-00 00:00:00','','uploads/a84aa6f67e302e0a89f7.jpg'),(54,'Add the delete functionion',1,'Projects are undeletable',47,12,'2016-08-06 15:20:23','2016-01-01',0,'',6,'9447fe3ecc057f6bfeab62de61a2635fc6756da4c09cf22a7b5111f6157e53','0000-00-00 00:00:00','','uploads/a84aa6f67e302e0a89f7.jpg'),(55,'Test task',1,'Haha',47,12,'2016-08-06 16:57:01','2016-12-31',0,'',6,'38ca2f09fdd5dd5b5fa992c8c4c6f3430fa5511517a07c23373a803fe45d07','0000-00-00 00:00:00','','uploads/2abb92fd872124b6820a.jpg'),(56,'Test task',2,'Haha',47,12,'2016-08-06 16:58:16','2016-12-31',0,'',6,'8a211c7ce56a5014eb466c4df7b6f57b3d3371a2cb4ecd30e1ebad0f521abc','0000-00-00 00:00:00','','uploads/2abb92fd872124b6820a.jpg'),(57,'Test task',2,'Haha',47,12,'2016-08-06 16:58:24','2016-12-31',0,'',6,'c2ae6ec1f972406cb901aed31a48e2c06c8d3de347c6254dfb926f0fcf535b','0000-00-00 00:00:00','','uploads/2abb92fd872124b6820a.jpg'),(58,'Test task',0,'Haha',47,12,'2016-08-06 16:58:24','2016-12-31',0,'',6,'6aab2ea9d4bfb9d66db509e4e39858330aff49b45ed30aa6fccbb84c363675','0000-00-00 00:00:00','','uploads/2abb92fd872124b6820a.jpg'),(59,'Test task',0,'Haha',47,12,'2016-08-06 16:58:55','2016-12-31',0,'',6,'de5b511d873f9a58911787fc66ba93f74de549d65b6546de8a526088f7c61a','0000-00-00 00:00:00','','uploads/2abb92fd872124b6820a.jpg'),(60,'Test task',0,'Haha',47,12,'2016-08-06 16:59:05','2016-12-31',0,'',6,'bd48f74a69729d92b5fdab21fa1da12786b4c89832129df44fd5eae1a9135a','0000-00-00 00:00:00','','uploads/2abb92fd872124b6820a.jpg'),(61,'Test task',0,'Haha',47,12,'2016-08-06 16:59:33','2016-12-31',0,'',6,'b64c0d3e33cda2af9042adfe239d3609388732abe538a349f06345f64ae7af','0000-00-00 00:00:00','','uploads/2abb92fd872124b6820a.jpg'),(62,'Test task',0,'Haha',47,12,'2016-08-06 17:00:55','2016-12-31',0,'',6,'d3221060b8da7965e00920a603021385938c03b993b239602c9a6f76485304','0000-00-00 00:00:00','','uploads/2abb92fd872124b6820a.jpg'),(63,'Test task',0,'Haha',47,12,'2016-08-06 17:01:03','2016-12-31',0,'',6,'aaeca036a4100a7316b74648fa1d57903be4914a870f950006dcd04c88cdea','0000-00-00 00:00:00','','uploads/2abb92fd872124b6820a.jpg'),(64,'Test task',0,'Haha',47,12,'2016-08-06 17:01:21','2016-12-31',0,'',6,'d91b2a9c140dfe58a5f7a2cc6e6870503b9249e55f6f6bca7c9d08feba81ae','0000-00-00 00:00:00','','uploads/2abb92fd872124b6820a.jpg'),(65,'Test task',0,'Haha',47,12,'2016-08-06 17:02:01','2016-12-31',0,'',6,'5755f5974d9a3083132dcae84f199d786938fe733eba77e361762f52dc827c','0000-00-00 00:00:00','','uploads/2abb92fd872124b6820a.jpg'),(66,'Test task',0,'Haha',47,12,'2016-08-06 17:02:39','2016-12-31',0,'',6,'ba12a4b04626487cc708a190867ce1ef30ab1f179d258bd8ce04909a0942e9','0000-00-00 00:00:00','','uploads/2abb92fd872124b6820a.jpg'),(67,'Test task',0,'Haha',47,12,'2016-08-06 17:05:46','2016-12-31',0,'',6,'cb92e377dd4901cb53e21b9537386cebeac600cf3fe9da5798fb1f762c71ae','0000-00-00 00:00:00','','uploads/2abb92fd872124b6820a.jpg'),(68,'Test task',0,'Haha',47,12,'2016-08-06 17:06:08','2016-12-31',0,'',6,'432034f7850b88d4c37af8e35b31dcac6617673a3c567d5623aaaed9b6174e','0000-00-00 00:00:00','','uploads/2abb92fd872124b6820a.jpg'),(69,'Test task',0,'Haha',47,12,'2016-08-06 17:08:40','2016-12-31',0,'',6,'d3d52cca3439ee7647d614a0fa9521618142b289763eb659f5092148d50c84','0000-00-00 00:00:00','','uploads/2abb92fd872124b6820a.jpg'),(70,'Bluh',0,'var taskForm 		= document.getElementById(\'task-form\'),\n		taskCreator 	= document.getElementById(\'task-creator\'),\n		flowZone 		= document.getElementById(\'flow-zone\');\n\n	taskForm.addEventListener(\'submit\', function(e){\n		e.preventDefault();\n\n		var taskName 		= id(\'task-name\').value,\n			description		= id(\'task-description\').value,\n			owner 			= id(\'assign-owner\').value,\n			dueDate 		= id(\'task-due-date\').value,\n			feat_img 		= taskForm.querySelector(\'.image-hash\').value,\n			priority 		= id(\'task-priority\').value;\n\n		if(feat_img == null){\n			feat_img = \'\';\n		}\n\n		var data = {\n			\'name\' 			: taskName,\n			\'owner\'			: owner,\n			\'description\'	: description,\n			\'priority\'		: priority,\n			\'due\'			: dueDate,\n			\'feat_img\'		: feat_img,\n		}\n\n			post(handlers + \'task.php\', data , function(response){\n				closeModal();\n				flowZone.innerHTML += response;\n			});\n\n	});',6,12,'2016-08-06 17:21:17','2016-01-01',0,'',6,'d0ca051661cff1ff79bfb1855d3480a2a7471c6fdeb09ed43112903619e984','0000-00-00 00:00:00','','uploads/3f195bb3041bcc1208a7.jpg'),(71,'Summer day',0,'var taskForm 		= document.getElementById(\'task-form\'),\n		taskCreator 	= document.getElementById(\'task-creator\'),\n		flowZone 		= document.getElementById(\'flow-zone\');\n\n	taskForm.addEventListener(\'submit\', function(e){\n		e.preventDefault();\n\n		var taskName 		= id(\'task-name\').value,\n			description		= id(\'task-description\').value,\n			owner 			= id(\'assign-owner\').value,\n			dueDate 		= id(\'task-due-date\').value,\n			feat_img 		= taskForm.querySelector(\'.image-hash\').value,\n			priority 		= id(\'task-priority\').value;\n\n		if(feat_img == null){\n			feat_img = \'\';\n		}\n\n		var data = {\n			\'name\' 			: taskName,\n			\'owner\'			: owner,\n			\'description\'	: description,\n			\'priority\'		: priority,\n			\'due\'			: dueDate,\n			\'feat_img\'		: feat_img,\n		}\n\n			post(handlers + \'task.php\', data , function(response){\n				closeModal();\n				flowZone.innerHTML += response;\n			});\n\n	});',6,12,'2016-08-06 17:22:32','2021-01-03',0,'',6,'f1c8201fa07adeebd705bed05cae35348b41912cb60788eb3baf34a19adb13','0000-00-00 00:00:00','',''),(72,'Edinburgh',0,'Wow the rain is pretty cool today',6,12,'2016-08-06 17:28:36','2016-01-01',0,'',6,'3a1884c30abcd4199a4e44e8ca213b4dc36c19e02f31a08f28f1cd25d617e3','0000-00-00 00:00:00','','uploads/9a0c36d6eb339b215256.jpg');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uploads`
--

DROP TABLE IF EXISTS `uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(400) NOT NULL,
  `mime_type` varchar(10) NOT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hash` varchar(255) NOT NULL,
  `sizes` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uploads`
--

LOCK TABLES `uploads` WRITE;
/*!40000 ALTER TABLE `uploads` DISABLE KEYS */;
INSERT INTO `uploads` VALUES (6,'uploads/2b0ec7fb0c496a02ba7b14faaa5fd4e60daec4fb.jpg','jpg','2016-06-09 10:28:22','fdc3530ae8dc41c5ff8d301a0099711ea7024739004e9c04d18cfbb11328bd56fb4ffbe00c2420ffb4799767fcb9cb188d884c040e0ac3462faccceb6b66a52f',''),(7,'uploads/640557a7c9184fd9f8153153ad9d46ba5ec5e1cb.jpg','jpg','2016-06-09 10:45:03','61cf5f72d3ea7b31d5418cdd3d5509cc37d785b836920243b8ac248489bd7b15458b17f5b7369928f2d0f056aa7d7c076ad80f01952cb32ae54e17dbca570ce0',''),(8,'uploads/a3e4a05f7361c8ca57b5e1bcb2b75f308520533e.jpg','jpg','2016-06-09 10:46:00','ab4d6c8ad73440bf18e728c41bc6c08fc0ba4c8076420fa6a295e9ebc386b51052ef5187687f197cbbd70f43bb4f9f55d8cda6e8f2a1a7f6cf98283d38fcd757',''),(9,'uploads/024a8dd6af5b5009bdd63b97842d06d2363aa746.jpg','jpg','2016-06-09 10:46:22','de0c83ce7f817664c168a362136981b1afc19d859120a69611a41cddc78e16f569a6269512ec2b8f3d365733533c0bcc00ee28867d752f48cfd3158ab9a79feb',''),(10,'uploads/0c9b5eaf2eb766080db86b8f5f854228a5eb039f.jpg','jpg','2016-06-09 11:10:41','1818841c64018f23bfe922f1ada12d43853f89b795a647badb5c70d0995cc9a165ad60aa8e91bd5dc21dd5b596c6109ed9f0e7ff2f0a19275b2a081a9182fa45',''),(11,'uploads/b9f608403def219750394198ee702e5681dd77ac.jpg','jpg','2016-06-09 11:12:06','4d5a71039e56be3f6682e3ebb4a0200794eefb52ddc9649ba19640278c00f3bdda940932d7447b96f969125891d915bb688c0e94128a99bf11036a5e850449a3',''),(12,'uploads/efcde4e7b150b43eb9808745f10c08d7a8f4bc81.jpg','jpg','2016-06-09 11:13:05','9cea6bf2c7a0b14e5726d8a071b15002165407ef9bd0c69fdc806629fb91ce97499e9aac2fa0b0e666fbe79f2e4034be385f54bbe098e76ec81baf1c635915ec',''),(13,'uploads/2e1898c6b5fc6a95d5472d8d64ceabc9200cd4ef.jpg','jpg','2016-06-09 11:16:56','72950a4143f0c6f5ea9393809aa7f88dffa95c0e32cb4dc28904e61a1a87347f384b5f1f9858d1ddf78b9e4814c4aa9ddd7687d30904887e359677f22656c9cb',''),(14,'uploads/0e4d8d6f813b328d4a3f23c12cf8e5577d579542.jpg','jpg','2016-06-09 12:44:47','a403ce956a54524f55144e142574e15d6b58084512ecad1cc303022caf40fd1689bd37440d93ad061a7a3ebb4459c181bf7c733be395da21fef47896630f00dd',''),(15,'uploads/8795002b1e627e73ec57a6a89fb72a9c30cd54d7.jpg','jpg','2016-06-09 13:07:33','92020a97d4d5fd91dbe55ca03723a08380c81ef415d14811c678c0c3598b30e485d6c9c9777fb4bcbb0bcd7691f98c405bffcc886c9012fa19b84576773b1666',''),(16,'uploads/c5b75cb2ba4ddd1abe211f214548420cdc9bbb8c.jpg','jpg','2016-06-09 13:08:08','2db67829b1aea93f2f3663970b189b1ff3dc3208e8a057e6f099a8939e18cc170487928aa66e39f9af4da0943e128720a1aa23128fdad2f30d7f22dac9061bfc',''),(17,'uploads/bb79990f726eef008532260a686552ea7b4d6d70.jpg','jpg','2016-06-09 13:10:05','298d7ab1253dba200eda2ebbad7a5fea0652040e05f48e1409434852712aec5df3e71877de2475aa4f5e76b0195e110467b7bc1edb68d58118d06cb89000ba6f',''),(18,'uploads/c966168f33a4395a4984b0e222178bd3fd7c08f1.jpg','jpg','2016-06-09 13:14:06','5237c33f73c82d1491b23e87be5cc103e99b2bf889fc350fb2a4b84ecdf9a4997f9dacf23dd61e59ab9ecc60bcdfb0ad0641504f786cc01ee8c078407f483d99',''),(19,'uploads/413f641fb9bfa2fc4fe5800f638746207a621b06.jpg','jpg','2016-06-09 13:16:49','b4552e020dd55db15a4142372cc1431b01f7db86d9ee4e7ec12037611c72d314db209fc726fb5d5f144172efbbfcc1d49f46f99eda84163cf290ad7e99da392b',''),(20,'uploads/678eedaa74fe2dda4bfccb6eb1629627a591b3c8.jpg','jpg','2016-06-09 13:58:06','9964f16c3bf5f5fd610eaac380abf7f5cdabc4b0dfc3dff2c3440ef9c967339176879523c45503124f0661df9ca298a8b51cf2ec7970a1f39ca8daddacf47fae',''),(21,'uploads/fde19d1e8f35320e75200eed73f567d5d3cef8e5.jpg','jpg','2016-06-09 15:55:47','6bfdc6e88ca27469a4de412f693e36eb0f176b2305a1233ca2aa5189466a3edf84c24d72c9911529ec16a0102108215698d65a79f666607bf06d49336a53a46c',''),(22,'uploads/4b69657ef63485529ae5e94e2838319588315207.jpg','jpg','2016-06-10 08:24:19','2c3d50c6f4c0fed287d5113870bc86625766898a6d9331a581bc85d93741828f15c3b0b692df2d4242aadc0d5ee7248cd789e14427d44c0923a46265b07f8567',''),(23,'uploads/3f0256944ba32f3e01e92b67d54807402443420b.jpg','jpg','2016-06-11 12:35:46','d27cc60dd2e1032d29b96cafdabd4acf2e77dabab1f91b0918093e7f6fb9693c8233579c263850c2a8a2367ab3b1c081e67ec6e864b37e77f83cd6e589c50fd9',''),(24,'uploads/5b8c1186ab612e4d55c2c62924410f656cbea8ad.jpg','jpg','2016-07-24 08:50:51','77680fb999b1517b454f279eeb6d47c7b0f538c8408552cf1bb262435cf2feeb06f8401f12f3d880ecac6db7cf48b65c2ec13ac34153ffc9a03904955116bec4',''),(25,'uploads/255326b62ff15ccb177570b94d5f1c5370e613cb.jpg','jpg','2016-07-24 08:52:53','f3ee9201b38fa2c29a8638f4844b4a230409da49b40e8d1da632a686596b698542dbb7ade908b2e53c0c8fc66e4e00e8d2fc66d1421016e55e1a50dd7b562943',''),(26,'uploads/d8f03e7935c0c81f4e9e884fd8a432bb95d1ba08.jpg','jpg','2016-07-24 09:32:19','f2cc2b79960f7c04b4a1e28c32210a67b4a228d12f211d3266b0ffc920d593281c5ccba6a4ef701504b5a1ccd91b568831d7806b1741e5e649ea34efc6106a84',''),(27,'uploads/362e09253ce78706d15f47b6ca6c011cc3554366.jpg','jpg','2016-07-24 09:35:58','cb0d0e105a22cfbfb12715b6fd93fa13f6660a50bd51fb90cce226c0bd0215114124bca836f0553372f1e2e67d7a9634de7e30340d584a8c65800350a97f21b2',''),(28,'uploads/8e81c155a6230c40c54e679c6437c8c29a1afaca.jpg','jpg','2016-07-24 09:43:25','16518354041663abe390bfd3e0d478f0469c347aeb162c0c8122f8c49a64c65429f3314f677d26f0b474db071aabec97602161a4dd2912beb68d0fb6092e7ff4',''),(29,'uploads/820ebabb4b00ecf3579070ee65b6ec1ee90ae41d.jpg','jpg','2016-07-24 09:44:11','16c0a0f568c73ed67ca86e065dbac2521592fb07636a8bf4e6b0682bee65f3eb1d9eb67e24945c59a9190925df5bdabbce31c7bb3913ec713c2eb30914f187a7',''),(30,'uploads/1b3784c3d786c706cf1980720c30c7d187e8d4c1.jpg','jpg','2016-07-24 09:53:05','acfb7fd7812fd6438a2be201dc147cf430d2cdfc048b4d34ce6150306d3d6864c8353281fe7a83546c7c3712bc81c2e7436caba2f44737d6dd1e622409412ef0',''),(31,'uploads/b9f4e8ae26e8d90d250959e4980c192f73c23c02.jpg','jpg','2016-07-24 09:54:32','26c33866b38b3ae7ef4b327b02ebe7c52cd62aa7468cabe2c3b13caff6b4f0ca8b6a021895567472b9a6e00190a90e679769f46c78b0cdcad52875a3029e3884',''),(32,'uploads/59bfbdc660d0f2a50a01ca16e466c67724b22e7b.jpg','jpg','2016-07-24 09:55:41','ab78d5435e1bde92e6319f20abb9141fba975f90e28bc3dc741f7172709ca9b7bdc52f2fcf0035deab32b9e5728d680ceb6fa529a5c61ecca08706a6094da90b',''),(33,'uploads/fee716e6e292e694a6ccd5bafcaad78de7aea957.jpg','jpg','2016-07-24 09:56:29','22496d599506f173e77053cafe88153d616b37fcfb46b2e64852b4fa4f9f0c65a05899fa87267416ef8474540837892dd516376d54a4526f19b5fd77b9803016',''),(34,'uploads/effd9caf0e4f0a0e3ba8ab1fec2c51644a0cfba7.jpg','jpg','2016-07-24 09:57:44','5bfa20598051c536d694b260a2398c33c0dae32c1eba58e81cf80ceeb441ce548c8185ad6c59ab0217575541b6916d60c50d161c4f875ffd11d70439bfcdfb94',''),(35,'uploads/4a0642b1ab47eeb743002d6d9716e82d1a946f4a.jpg','jpg','2016-07-24 09:58:45','5d08a9ace78de1fa8dd9948968511d7666f03bc900794afd677ed01b066e426db4d33a94b07cb6f04077a904969e028f92ce26ceba10230eabf8dfc3c1bc13d7',''),(36,'uploads/7e85dc32f909e564336c90913aee6b51a8d7859d.jpg','jpg','2016-07-24 10:00:00','d5e688fc1c6ae9837d780799982bb0c2e408fb33cd60850bbc0a27c642aa9f47d8a92f5e3cbd58c550f49a4ec2e8db94df2761f13e5cbba50ea9267d8762d22e',''),(37,'uploads/4d6bf09205cb7f64bd29f2dc596c152471c58f2a.jpg','jpg','2016-07-24 10:01:13','a045ea6388d38130d0db8370ebefd2b0a049cfc587028294edf61e872fade53d8fdfb3e6662180c0f184f67867f67ce1277d3fa24e2e289125f078cdee5d39c8',''),(38,'uploads/54369844a3620efa7bfed213e3559af0efb4d2f5.jpg','jpg','2016-07-24 10:02:51','bbf65e563c2feeeeb851d6902cc4487748c45f49c52e90343e0bb1363f9fcd285272ae7c1ea09d587645c7bbf2c9b75897e6a54a78cc940b47cca6c68cd91014',''),(39,'uploads/74ffdf68020b08d7256701a8cb1694d1fe31d3b8.jpg','jpg','2016-07-24 11:16:13','e848996bbefd5c15563ad002bdbfbe996010fedb78f922095d579553086f0b2e4140f916c6e1b07e17fb99fae07b3283f06a6f66e3e2b01524dac7338132cb04',''),(40,'uploads/30a85aef9b7b61666e6700c099d33641a4e5ae47.jpg','jpg','2016-07-24 11:16:57','19579445e3296ec2f67a1ff380753b33730edd924d2d56dab07ff3f423ad28391781963b786c17672ad4181a84bdb171ee46b2ff36d4eaff8e62484443e3f805',''),(41,'uploads/d2b15dd89737f7de28192186922d22402823e9b5.jpg','jpg','2016-07-24 11:17:57','e20befcc0da833d0c383f773e1fe6cfc01d521bd774dd9706e02d565eb4460faeb426c76cde4105e060c1aeaac83900773110d763b13664707f41c68331a1fc1',''),(42,'uploads/58883770c2536e8484fd4e363c194a5b56cd6cdd.jpg','jpg','2016-07-24 11:19:36','7d615db9673c8491dd0ec1ce527806090c27c3dc9dfddb7a09adcea56bd0fffddc3f9bdea27f6547a933e53338f763fd4023e9ab9ffbd9906bd164b504e8bb83',''),(43,'uploads/e8574b6f1a9119d5f3f4609073c408225c8a4637.jpg','jpg','2016-07-24 11:21:00','c48e92df20d53d7137c9e01ae7bf57b0f3093e610b906a39aeeac6627c5860be1b7eb4fc8ad120d5e55e47f2613d50d39af3c7e28c579ad491f1eaf4d38e63b2',''),(44,'uploads/bc573fd1550b9781e87e611929c58a0d0a128eab.jpg','jpg','2016-07-24 11:22:17','68b4c417689730afdd0be2328a709c82ca991790b1dbceb409de7f702f0c8467b70bef9784b9c592a1ff1dec54ec9dfdeffe0b4913ba267a20a693863d8004bb',''),(45,'uploads/9d4122a18c88a1ca7c6ab56f42089c55eb13c70b.jpg','jpg','2016-07-24 11:27:06','a674adbabc9ac4df2f0b9f0240368da91ff8415f11f65344e75b86fe4ab343a2dc22c2bffad9c34e63c3305d99d1fd3b074aa84b0fc1b05139ef5a6f878d975a',''),(46,'uploads/210f58352cc571ddf689e3146f92619e1c43f80d.jpg','jpg','2016-07-24 11:38:36','98d803f46d2116f621d9eb0bfd14a152f521df6e7eca5c397c6c70cbd1b5749590ac704d2f745abd877556eb351f6c90fa37a2b23c5dd0f7e309acd2327be628',''),(47,'uploads/ed80926970164f17fbfc69cd311f0da1ede8a0f3.jpg','jpg','2016-07-24 11:47:25','d91a9a972b68787a96d9487365f042e3a68a7dc169eaedfeae7220651f3064bdd8ba92e12e9f1a6ff6e31d67b44cefefa9b236c12bd763979a0c7b1ec2c2296b',''),(48,'uploads/354f1fa0463cd0e57e47171e0da9801a383a9467.jpg','jpg','2016-07-24 11:56:19','8a4372a28e67cdd4f5544d130cfde86e26143f56c20e91faeff31b9d3f7fbb1377d55aeb5d17fa5b38ab5e979d0d79f9c5f96e3935cff7b933edf89a6aa70834',''),(49,'uploads/c6d1f74308099b15f4143bbea3522347b80c734d.jpg','jpg','2016-07-24 12:00:10','80c2ddc15036d6e4bef759b34aeb3c90c9a5dade06ed40952621d61c0ef3d2d550f984bc2eb6a33de4d0a10f286985a8cf0d3771712c4be4e79a3abab3f71b4c',''),(50,'uploads/79e6c23d6b1189b95c33da5d20bafd3073881d9d.jpg','jpg','2016-07-24 12:10:21','b23afa5710185e5e18bf827ae135f2a606dfd59fdecdef52a750f154215276948bcefd47254965fe4f69d7a93511b4adb4ac657a862ecf6138f53a7e8b110b9a',''),(51,'uploads/be10e5aa48461db7e13d2f3557de1e6ad94c446a.jpg','jpg','2016-07-24 12:17:00','470c0b987ca3fe339f8de47387476350195effd61ede3281a6dd52b29a4d3bd1a1dea647b8cbb304db5553d2f84b7f1948fe669100eb6f621ef7c9f74d14e51c',''),(52,'uploads/c2e81814d66c0a6bad2f83d33abfe6afb13d2ecf.jpg','jpg','2016-07-24 16:37:03','a3674a8dabebf0ba5d32983aa8e19d5662aa341d7515e9238658c87c886a75a1a2e3812f627437ae19aaed2367e254b573a600023cc6c3e89e008dc523ea429f',''),(53,'uploads/d9c21228e56512ef45ec57bc4619315387cc32e0.jpg','jpg','2016-07-24 16:40:13','c1632135a3528a1872cf546cd219b735f89fe554478c25224747b8dc962a9446161bab0b13fa3928b1b6977c9abc38dc53ccc42ce3c242d1e0c0f9b4a0272666',''),(54,'uploads/c3ed45eda7e795884b2eb7fc25aff4daaa33c991.jpg','jpg','2016-07-24 16:41:02','80bb42d018ecfe55fcbf33c60d6be32a079d7f3effecfeb0d329e1cc5bf8bf784adcf6482beb8cc76f92b4eca6c796891a68250c7320090045ba7b3e119cb348',''),(55,'uploads/2f70921cec3dde0bebb81967b8024a8549477b27.png','png','2016-07-24 16:50:13','cbd039bf01666c5710d4467dd09864be8cbda2c446940d5f302a7ccbdd09309bda8e989eb6ff842d7132765545ec5527b0b00b84e6399899433ddfb6b9551449',''),(56,'uploads/a227253eab47aa01a7bf710fbffebe28cbff0d0e.jpg','jpg','2016-08-06 12:06:54','74f3d3bc9607082d7f535f526df0ecb663a474c87406eab57ed1441640cb6e3a2faff768cbc0ee82b991717767a2a523bf009b3a35b83deb271806c07ae7c084',''),(57,'uploads/72f8d8217a9b2e1d90e57390980522a6673f0909.jpg','jpg','2016-08-06 12:40:00','d48d1794ef17f84a53b2a2ff1dbab39459ab4d5bf5651dee0d296a6f41344fdc1b06a5232bc7d87fb4e48bc21fada3c6d1aed0853307e7e93ecb61d6a91ae8b2',''),(58,'uploads/e8407791d266e65a841ec6344e0cb0107aa3df80.jpg','jpg','2016-08-06 12:40:30','958dafb406776c0a998c11acdcde8c8d550e27749924a37fa6e0f9cd15031dbbfa0ca671826184985adf3520e95fd00b8ba884c503257f515d8aefa67e9179c3',''),(59,'uploads/92f9a390af8c448b6f84a01e9561a075cacda665.jpg','jpg','2016-08-06 12:41:39','fea440ad2029162aba3efe3ac72941a8eb059564a04ff4263820ef0b704027670fa8ab58841cb300544b62ab59dd787182d5b4e6690996ec4e52255c4e97289a',''),(60,'uploads/0e4d4db9b7a42b15086ca22321b4c119fc0489dd.jpg','jpg','2016-08-06 12:43:42','500a63188bdf9b924d1105a616e134541e34546f538f876f8e239be2b5ffe11bfc83c46bc8ecc322190bde1be73f1b5ac4ace84069187cadcc9c436793f8ff35',''),(61,'uploads/6d2e00872371706b1c8d2551e4d9484731a33d02.png','png','2016-08-06 13:02:32','54b5623e79001a94abcca1b4b48879cb355d83a233d3e3b8b86a5cea91199f689839e9fa065ae5c722c6be1c83be49fae3f2ba2eab9de0f9c136d5137c11ed62',''),(62,'uploads/363530e93177d0a88c81.jpg','jpg','2016-08-06 13:09:46','56e0bbfcb0c05a6a3ef0',''),(63,'uploads/00d91b49670db44e4c09.jpg','jpg','2016-08-06 13:13:44','16ea05832b2fbf5b6c4b',''),(64,'uploads/2d3a58109f30979ecdf8.jpg','jpg','2016-08-06 13:19:34','180f50b39ac78af5d354',''),(65,'uploads/5809dc727b854af88e8a.jpg','jpg','2016-08-06 13:24:51','50787defe0ac62bf9168',''),(66,'uploads/56d6c744da8b25ccebfe.jpg','jpg','2016-08-06 13:29:41','3b74ece500b141dde39c',''),(67,'uploads/6aa69bc6506c87b61dfd.jpg','jpg','2016-08-06 13:30:36','fd32358020faf7146852',''),(68,'uploads/9df0b1fd7a2f7ce925d4.png','png','2016-08-06 13:37:38','ae04f3d9b8a3b27baaf6',''),(69,'uploads/dbfcb64c610f2a4a52de.jpg','jpg','2016-08-06 13:50:54','b70f8b36e23f1b7e4be5',''),(70,'uploads/a1dd5d25864c82972fe4.jpg','jpg','2016-08-06 13:51:47','79f953e04374e1a17ae5',''),(71,'uploads/eacb3f4c5c5566facab4.jpg','jpg','2016-08-06 13:57:47','5cf8b9b31f1f72631919',''),(72,'uploads/9fc401fe66145b58b113.jpg','jpg','2016-08-06 13:57:56','d1ae56cdab5b3ba12371',''),(73,'uploads/28f5e2e4a8a13a1dab02.jpg','jpg','2016-08-06 15:10:28','e184de77d18db45f27e0',''),(74,'uploads/8ff0bbe438f5f922b9e4.JPG','JPG','2016-08-06 15:10:36','515e5704d43deae9b52b',''),(75,'uploads/561e994354366a127e43.png','png','2016-08-06 15:13:24','49c4cad0f1ba945b7d19',''),(76,'uploads/a84aa6f67e302e0a89f7.jpg','jpg','2016-08-06 15:19:01','dfc6725008abb256caa5',''),(77,'uploads/2abb92fd872124b6820a.jpg','jpg','2016-08-06 16:56:24','ed965415d3334d2acf61',''),(78,'uploads/3f195bb3041bcc1208a7.jpg','jpg','2016-08-06 17:21:16','7870bce61c8b16143350',''),(79,'uploads/9a0c36d6eb339b215256.jpg','jpg','2016-08-06 17:28:22','77b237fd84a5fed8b575',''),(80,'uploads/004077e8d36d275e04be.jpg','jpg','2016-08-06 18:48:19','45a1de9cec551deac1cc',''),(81,'uploads/71a852ac4df168079dcb.jpg','jpg','2016-08-06 18:59:08','49ca22d78997e0819caf',''),(82,'uploads/463f146f6c6520a1b29e.jpg','jpg','2016-08-06 18:59:53','1ed7a0ce984af4027d76',''),(83,'uploads/01390379294d28d826bd.jpg','jpg','2016-08-06 19:02:08','0c5745c577ea9ef260e8',''),(84,'uploads/7d07882e7194d80b6344.png','png','2016-08-07 18:19:41','7aa6bb9ba4aa71e5b596',''),(85,'uploads/d64208a2388776041fa3.png','png','2016-08-07 18:42:55','94b1496ac6326297a195',''),(86,'uploads/7a5d5e108f862e1bf59f.png','png','2016-08-07 18:44:18','16f59c8823cd93a53ac6',''),(87,'uploads/01446f0bbfc4894bf3a0.png','png','2016-08-07 18:45:36','2e19222648b1ff4a4d70',''),(88,'uploads/1eef12760e4ca1db4f1d.png','png','2016-08-07 19:22:12','a1b323fe4f4a3262e7a5',''),(89,'uploads/34a51633c7a528c366bf.jpg','jpg','2016-08-08 08:52:24','253d517f10afa56a6c98',''),(90,'uploads/8e62ca8118665c1f05d7.jpg','jpg','2016-08-08 13:29:42','6a2d55da725045db7210',''),(91,'uploads/97c077a7105874434c6e.jpg','jpg','2016-08-08 14:11:27','3c6993024c6b31744e3f','');
/*!40000 ALTER TABLE `uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `surname` varchar(40) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(50) NOT NULL,
  `projects` varchar(500) NOT NULL,
  `activation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activation` varchar(76) NOT NULL,
  `password` varchar(255) NOT NULL,
  `color` varchar(7) NOT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `connections` varchar(5000) NOT NULL,
  `role` varchar(60) NOT NULL,
  `header` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (6,'ikethepike','Isaac','Kuehnle-Nelson','0000-00-00','ikethepike@outlook.com','[\"12\",\"27\"]','2016-06-07 21:39:57','a3116fcb0ff78581d441a3de68287e73','$2y$11$PzCnHSzYMOxP4/xLR71hmeGap/uEvpg/6OJslOInpNz.g8pjOMoYu','#fe495a','uploads/8ff0bbe438f5f922b9e4.JPG','{\"1\":6,\"0\":47}','',''),(47,'stinkysteve','Stinky','Steve','2025-01-01','stinky@steve.com','[\"13\", \"12\"]','2016-06-07 22:01:37','f6792d1d648ecf9addcc9b96ee13351b','$2y$11$.9c2BFHCThSZavgxOoz4lu5/d3TUKY2vyaggAlxFOlqYdLn8Xh.kq','#00a2d3','','','',''),(48,'lolo','lovisa','nelson','0000-00-00','pea_easy@hotmail.com','','2016-08-08 16:54:30','72b32a1f754ba1c09b3695e0cb6cde7f','$2y$11$tRDslo5JanwEkh3xBVSwzOvbpNzeQadUs0sz413jaHgqWT2R/BB9q','#0b0c90','','','','');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-17 17:41:46
