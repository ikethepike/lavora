<?php include 'inc/head.php'; ?>

</head>
<body id='register-page'>

<main>
	<div id="register-bar">
		<div id="register-wrapper">
			<header>
				<h1 class='title'>Register</h1>
				<span class="description">By registering you gain access to all the functionality that <?php echo $site_title; ?> has to offer</span>
			</header>
			<?php include 'modules/register.php'; ?>
		</div>

	</div>
	<div id="register-canvas">
	</div>


</main>

</body>
</html>