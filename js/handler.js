var handlers 		= "./handlers/",
	xhttp 			= new XMLHttpRequest(),
	pingback 		= null,
	image_hash 		= null,
	image_link  	= null,
	dropzone_dim	= null;


function insert(table, data){

	if(typeof data !== 'string'){
		data['table'] = table;
		post( handlers + "insert.php", data);
	}
	return false;
}

function project(data, callback){

	if(typeof data !== 'string'){
		
		post(handlers + "new_project.php", data, function(data){
			callback(data);
		});

	} else {
		return false;
	}

}

function task(data){
	
}


function update(data, callback){
	if(typeof data !== 'string'){
		
		post(handlers + "update.php", data, function(data){
			callback(data);
		});

	} else {
		return false;
	}

}


var completed = false;

function post(destination, data, callback){

	if(typeof data === 'string'){
		return false;
	}

	var data = JSON.stringify(data);


	xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
        	callback(xhttp.responseText)
        }
    };
		

	xhttp.open("POST", destination, true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(data);
	
}

// Update Data function

function updateData(table, where, data){

    if(typeof data !== 'string'){

        var toSend = {
        	"where" 	: where,
        	"table"		: table,
        	"contents"	: data,
        }

        post(handlers + 'update.php', toSend, function(data){
        	console.log(data);
        });
    }
}

function id(name){
	return document.getElementById(name);
}

// General Upload 

document.addEventListener("DOMContentLoaded", function() {

	var dropzones 		= document.getElementsByClassName('dropzone');

	for (var i = 0; i < dropzones.length; i++) {
		dropzones[i].ondragover = function(){
			this.className = 'dropzone dragover';
			return false;
		}
		dropzones[i].ondragleave = function(){
			this.className = 'dropzone';
			return false;
		}
		dropzones[i].ondrop	= function(e){
			e.preventDefault();



			var	image_hash		= this.querySelector('.image-hash'),
				image_link		= this.querySelector('.image-link'),
				dropzone 		= this;

			this.className = 'dropzone';

			upload(e.dataTransfer.files, function(data){
				dropzone.className 			= "dropzone dropzone-success";
				dropzone.style.background 	= "url('" + data[0]['url'] + "') no-repeat center";
				image_link.value			= "background: url(" + data[0]['url'] + ") no-repeat center";
				image_hash.value			= data[0]['hash'];
			});
		}
	}

});


function upload(files, callback) {
	var formData 	= new FormData(),
		xhr		 	= new XMLHttpRequest(),
		i;

		for(i = 0; i < files.length; i++) {
			formData.append('file[]', files[i]);
		}


		xhr.onload = function(){

			var data = JSON.parse(this.responseText);

			if(data.success){
				callback(data);
			} else {
				dropzone.style.border	= '1px dashed red';
			}
		}

		xhr.open('post', handlers + 'uploadhandler.php');
		xhr.send(formData);

}	