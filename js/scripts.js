document.addEventListener('DOMContentLoaded', function(){
	main();
});

function main(){


	// Opening and closing
	var close 	= document.getElementsByClassName('close-button'),
		modals 	= document.getElementsByClassName('modal'),
		pageDim = document.getElementById('page-dim');

	if(close !== null){

		for(var i = 0; i < close.length; i++){
			close[i].addEventListener('click', function(){
				closeModal();
			});
		}

	if(pageDim !== null){
		pageDim.addEventListener('click', function(){
			closeModal();
		});
	}

	var openLinks = document.getElementsByClassName('open-link');

	for(var i = 0; i < openLinks.length; i++){
		openLinks[i].addEventListener('click', function(){
			openModal(this.getAttribute('data-link'));
		});
	}


	var open = false;

	function openModal(name){
		pageDim.className = 'page-dim page-dim-active';
		open = true;
		document.getElementById(name).className += ' open-modal';
	}

	function closeModal(){
		pageDim.className = "page-dim";
		open = false;
		for(var i = 0; i < modals.length; i++){
			modals[i].className = modals[i].className.replace( /(?:^|\s)open-modal(?!\S)/g , '' );
		}
	}

	} // Modals check


	var hamburger 	= document.getElementById('hamburger'),
		userbar 	= document.getElementById('userbar'),
		userbarVis 	= false;

	if(hamburger !== null){
		hamburger.addEventListener('click', function(){
			if(!userbarVis){
				hamburger.className += ' active';
				userbarVis = true;
				openUserbar();
			} else {
				hamburger.className = 'hamburger-menu';
				userbarVis = false;
				closeUserbar();
			}
		});
	} // Null hamburger check

	function openUserbar(){
		userbar.className = "userbar userbar-open";

	}

	function closeUserbar(){
		userbar.className = 'userbar';
	}

	// Header Search

	var magnifyingButton 		= id('magnifying'),
		magnifyingExpansion 	= id('magnifying-expansion'),
		magnifyingOpen			= false;

	if(magnifyingButton !== null)
	{
		magnifyingButton.addEventListener('click', function(){
			if(!magnifyingOpen){
				magnifyingExpansion.className = 'mag-hidden mag-visible';
				magnifyingOpen			= true;

			} else {
				magnifyingExpansion.className = 'mag-hidden';
				magnifyingOpen			= false;
			}
		});
	} // End null check




	// Ajax link loading

	var ajaxLinks = document.getElementsByClassName('ajax-link'),
		ajaxZone  = id('ajax-zone');

	if(ajaxLinks !== null){
		for (var i = ajaxLinks.length - 1; i >= 0; i--) {
			ajaxLinks[i].addEventListener('click', function(ev){
				ev.preventDefault();

				var target = this.getAttribute('data-link');

				load(target, function(xhr){
					ajaxZone.innerHTML = xhr.responseText;
					pageDim.className = 'page-dim page-dim-active';
					draggable();
					main();
				});
			});
		}
	}

	// Settings page
	var settingsLink = document.getElementsByClassName('settings-link'),
		settingsAjax = id('settings-ajax');

	if(settingsLink !== null){
		for (var i = settingsLink.length - 1; i >= 0; i--) {
			settingsLink[i].addEventListener('click', function(){
				var pane = this.getAttribute('data-setting');
				load("inc/ajax/" + pane + ".php", function(xhr){
					settingsAjax.innerHTML = xhr.responseText;
				});
			});
		}
	}



	var tasks = document.getElementsByClassName('task');

	// Jazzy effects on task list 
	if(tasks !== null){
		for(var i = 0; i < tasks.length; i++){
			tasks[i].addEventListener('mouseover', function(){
				this.className += ' hover';
			});
		}
		
	}


	// Popping up assigner in task view

	var addMemberButton = id('task-add-member'),
		assignerWrapper = id('assigner-wrapper');

	if(addMemberButton !== null){

		var assignerOpen = false;

		addMemberButton.addEventListener('click', function(){
			if(!assignerOpen){
				assignerOpen = true;
				assignerWrapper.className += ' assigner-open';
			} else {
				assignerOpen = false;
				assignerWrapper.className = assignerWrapper.className.replace( /(?:^|\s)assigner-open(?!\S)/g , '' )
			}
		});
	}



// Profile section

var avatarForm = id('change-avatar-form');

if(avatarForm !== null){

	avatarForm.addEventListener('submit', function(ev){
		ev.preventDefault();

		var hashValue = avatarForm.querySelector('.image-hash').value;

		var data = {
			table		: 'users',
			hash 		: hashValue,
			where 		: ['id', 6],
			field 		: 'profile_pic',

		}


		post('handlers/hash_update.php', data, function(responseText){

			var response = JSON.parse(responseText);

			if(response.status == true){
				window.history.back();
			}
		});


	});
}



var taskForm 		= document.getElementById('task-form'),
	taskCreator 	= document.getElementById('task-creator'),
	flowZone 		= document.getElementById('flow-zone'),
	taskExtra 		= document.getElementById('extra-header');

if(taskForm !== null){
	
	taskForm.addEventListener('submit', function(e){
		e.preventDefault();

		var taskName 		= id('task-name').value,
			description		= id('task-description').value,
			owner 			= id('assign-owner').value,
			dueDate 		= id('task-due-date').value,
			feat_img 		= taskForm.querySelector('.image-hash').value,
			priority 		= id('task-priority').value;

		if(feat_img == null){
			feat_img = '';
		}

		var data = {
			'name' 			: taskName,
			'owner'			: owner,
			'description'	: description,
			'priority'		: priority,
			'due'			: dueDate,
			'feat_img'		: feat_img,
		}

			post(handlers + 'task.php', data , function(response){
				closeModal();
				flowZone.innerHTML += response;
				main();
			});

	});
	
	var extraOpen = false;

	taskExtra.addEventListener('click', function(){
		if(!extraOpen){
			extraOpen = true;
			taskCreator.className += " expanded-task-creator";
		} else {
			extraOpen = false;
			taskCreator.className = taskCreator.className.replace( /(?:^|\s)expanded-task-creator(?!\S)/g , '' )
		}
	});


}


// Task creator





}; // End of Main 

// Reusable

function id(name){
	return document.getElementById(name);
}





function upTo(el, tagName) {
  tagName = tagName.toLowerCase();

  while (el && el.parentNode) {
    el = el.parentNode;
    if (el.tagName && el.tagName.toLowerCase() == tagName) {
      return el;
    }
  }

  return null;
}

function load(url, callback) {
    var xhr;

    if(typeof XMLHttpRequest !== 'undefined') xhr = new XMLHttpRequest();
    else {
        var versions = ["MSXML2.XmlHttp.5.0", 
                "MSXML2.XmlHttp.4.0",
                "MSXML2.XmlHttp.3.0", 
                "MSXML2.XmlHttp.2.0",
                "Microsoft.XmlHttp"]

        for(var i = 0, len = versions.length; i < len; i++) {
        try {
            xhr = new ActiveXObject(versions[i]);
            break;
        }
            catch(e){}
        } 
    }
        
    xhr.onreadystatechange = ensureReadiness;
        
    function ensureReadiness() {
        if(xhr.readyState < 4) {
            return;
        }
            
        if(xhr.status !== 200) {
            return;
        }


        if(xhr.readyState === 4) {
            callback(xhr);
        }           
    }
        
    xhr.open('GET', url, true);
    xhr.send('');
}

function getScript(source){
	var resource = document.createElement('script'); 
	resource.src = source;
	var script = document.getElementsByTagName('script')[0];
	script.parentNode.insertBefore(resource, script);
}

function draggable(){
	var taskLists = document.getElementsByClassName('task-listing');

	if(taskLists !== null){
		for(var i = 0; i < taskLists.length; i++){
			Sortable.create(taskLists[i], { 
				group: "sorting",
				onAdd: function (ev) {
						var parent 		= ev.to,
							item 		= ev.item;
							itemId		= item.getAttribute('data-id'),
							status 		= parent.getAttribute('data-status'),
							listing 	= document.getElementById('listing-'+status);

							listing.className = listing.className.replace( /(?:^|\s)inactive(?!\S)/g , '' );

							var data = {
								'status'	: status,
							}

							updateData('tasks', ['id', itemId], data, function(){
							});

				},
			});
		}
	} // Tasklists null check
}