<?php
	// Setup Globals

	$site_title = "Do-Do";


	// Constants

	define('BASE_URL', realpath(__DIR__ . '\..') . '\\');
	define('HOME_URL', 'http://localhost/lavora'); 


	// Class settings

	session_start();

	$GLOBALS['config'] = array(
		'mysql' => array(
			'host' 		=> '127.0.0.1',
			'user' 		=> 'root',
			'password'  => '',
			'db'		=> 'lavora'
			),
		'cookie' => array(
			'cookie_name' 	=> 'r_hash',
			'cookie_expiry' =>  2678400 // Month
		),
		'session' => array(
			'session_name' 	=> 'user',
			'token_id'		=> 'token',
		),
		'passwords' => array(
			'standard'	=> PASSWORD_BCRYPT,
			'cost'		=> 11,
		),
		'uploads' => array(
			'dir'			=> BASE_URL . "uploads/",
			'image_ext'		=> array('jpg', 'png', 'gif', 'bmp', 'svg', 'jpeg'),
			'file_ext'		=> array('txt', 'mp4'),	
		),
	);

	// Autoloading Class files

	spl_autoload_register( function($class){
		require_once BASE_URL . 'classes/'. $class . '.php';
	});

	require_once BASE_URL . 'functions/sanitize.php';

	// Checking for cookie

	$cookie = Config::get('cookie/cookie_name');


	if(Cookie::exists($cookie) && !Session::exists(Config::get('session/session_name')) ){
		$hash = Cookie::get($cookie);

		$check 	= DB::getInstance()->get('user_sessions', array('hash', '=', $hash ));

		if(!empty($check)) {

			$user = new User($check->first()->user_id );
			$user->login();
		}
	}


