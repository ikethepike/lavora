<?php 

class Api{
	

	private static $_session;

	private $_db;

	protected $_request = null,
			  $_method;


	public function __construct($token = null){

		$this->_db = DB::getInstance();


		if(!isset($token)){

			// Bind var to Hash
			$this->_token = Hash::make(72);

			// Insert into applications
			$this->_db->insert('applications', array(
				'token'		=> $this->_token,
			));

			return true;
		}

		if($this->tokenCheck($token)){
			return true;
		}

		return false;
	}

	public function action($url){
		$this->_args = explode('/', rtrim($request, '/'));		
		$this->_method = $_SERVER['REQUEST_METHOD'];

	}

	private static function tokenCheck($id){

		if(!isset($id) || !isset($token)){
			die('Missing ID or Token');
		}

		$result = $this->_db->get('applications', array('token', '=', $token))->first();


		if($result->hash == $this->_token){
			$this->_session = $result;
			return true;
		}

		return;
	}

	public function insert($table, $where){
		$this->_db->insert($table, $where);
	}

	public function delete($table, $where){
		$this->_db->delete($table, $where);
	}

	public function get($table, $where){
		$this->_db->get($table, $where);
	}

	public function update($table, $where){
		$this->_db->update($table, $where);
	}


}