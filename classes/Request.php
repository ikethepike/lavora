<?php 


/**
* 
*/
class Request
{
	
	private $_url,
			$_error = false,
			$_response;


	public function post($url, $data){

		$this->_error = false;

		$this->_url = $url;
		if(!is_array($data)){
			$this->_error = true; 
			return;
		}

		Self::curlHandler('post', $data);

	}


	public function get($url){
		$this->_url = $url;

		if(empty($url)){
			$this->_error = true;
			return;
		}

		Self::curlHandler('get');

	}

	private function curlHandler($method, $postData = array()){

		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $this->_url);

		if( strtolower($method) == 'post'){
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($postData));
		}

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$this->_response = curl_exec($curl);
		curl_close($curl);

	}

	public function response(){
		return $this->_response;
	}

}