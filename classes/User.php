<?php

class User {
	private $_db,
			$_data,
			$_isLoggedIn,
			$_cookieName,
			$_sessionName;


	// Instantiate DB
	public function __construct($user = null) {
		$this->_db = DB::getInstance();
		$this->_sessionName = Config::get('session/session_name');
		$this->_cookieName 	= Config::get('cookie/cookie_name');


		// Find current user
		if(!$user) {
			if(Session::exists($this->_sessionName)) {
				$user = Session::get($this->_sessionName);

				if($this->find($user)){
					$this->_isLoggedIn = true;
				} else {
					// logout
				}
			}

		// if user is set
		} else {
			$this->find($user);
		}
	}

	// Create new user
	public function create($fields = array() ) {
		if( !$this->_db->insert('users', $fields) ){
			throw new Exception('Problem creating user');
		}
	}

	// Logging in users

	public function login($username = null, $password = null, $remember = false){

		if(!$username && !$password && $this->exists()) {
			Session::put($this->_sessionName, $this->data()->user_id );
		} else {

			$user = $this->find($username);

			if($user){

				if( password_verify( $password , $this->data()->password ) ){
					Session::put($this->_sessionName, $this->data()->id );

					if($remember){

						$check 	= $this->_db->get('user_sessions', array('id', '=', $this->data()->id ));

						if(!$check->count()){

							$hash 	= Hash::make(72);

							$this->_db->insert('user_sessions', array(
								'id' 	=> $this->data()->id,
								'hash'		=> $hash
							));

						} else {
							$hash =	$check->first()->hash;
						}


						Cookie::set($this->_cookieName, $hash, Config::get('cookie/cookie_expiry'));

					}

					return true;
				}


			}

		}

		return false;
	}


	// Find user by ID or by Username, return user object

	public function find($identifier = null){
		if($identifier){
			$field 	= (is_numeric($identifier)) ? 'id' : 'username';
			$data 	= $this->_db->get('users', array($field, '=', $identifier ));

			// records found
			if($data->count()){
				$this->_data = $data->first();

				return true;
			}

		}
		return false;
	}


	// Data returning functions
	public function data(){
		return $this->_data;
	}

	public function isLoggedIn(){
		return $this->_isLoggedIn;
	}

	public function logout(){

		$this->_db->delete('user_sessions', array('id', '=', $this->data()->id ));

		Session::delete($this->_sessionName);
		Cookie::delete($this->_cookieName);
	}

	public function exists(){
		return (!empty($this->_data) ) ? true : false;
	}

	public function friends($memberId){

			$friends = (array) json_decode($this->_data->connections);

			$status = false;

			if(in_array($memberId, $friends)){
					$status = true;
			}

			return $status;

	}

}