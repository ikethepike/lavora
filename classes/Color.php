<?php 

class Color{

	// Generate random hex color
	public function random(){
		$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
	    return $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
	}


	public function hex2rgb($hex) {
	$hex = str_replace("#", "", $hex);

	if(strlen($hex) == 3) {
	   $r = hexdec(substr($hex,0,1).substr($hex,0,1));
	   $g = hexdec(substr($hex,1,1).substr($hex,1,1));
	   $b = hexdec(substr($hex,2,1).substr($hex,2,1));
	} else {
	   $r = hexdec(substr($hex,0,2));
	   $g = hexdec(substr($hex,2,2));
	   $b = hexdec(substr($hex,4,2));
	}
	$rgb = array($r, $g, $b);

	return $rgb; 
	}

	public function shift($hsl, $percent, $output = 'rgb'){

		if($percent < 1){
			$percent = $percent * 100;
		}

		if(!is_array($hsl)){
			$hsl = Self::rgb2hsl( Self::hex2rgb( $hsl ) );
		}

		$hsl[0] = $hsl[0] * ($percent / 100 );
 		

		if($output == 'rgb'){
			return Self::hsl2rgb($hsl); 
		}
		return Self::rgb2hex( Self::hsl2rgb($hsl) );
	}

	public function darken($rgb, $decimal) {

	if(!is_array($rgb)){
		$rgb = Self::hex2rgb($rgb);
	}

	foreach($rgb as $color) {
		 $output[] = round($color * $decimal);
	}

	return "rgb({$output[0]}, {$output[1]}, {$output[2]})";
	}

	public function lighten($rgb, $decimal) {

	if(!is_array($rgb)){
		$rgb = Self::hex2rgb($rgb);
	}

	foreach($rgb as $color) {
		$output[] = round($color / $decimal);
	}

		return "rgb({$output[0]}, {$output[1]}, {$output[2]})";
	}

	public function rgb2hsl( array $rgb) {


	$r = ($rgb[0] / 255);
	$g = ($rgb[1] / 255);
	$b = ($rgb[2] / 255);

	$min    = min($r, $g, $b);
	$max    = max($r, $g, $b);
	$delta  = $max - $min; 
	$luma   = ($max + $min) / 2;

	if($delta == 0) {
	$hue = 0;
	$sat = 0;
	} else {

	($luma < 0.5) ? $sat = $delta / ($max + $min) : $sat = $delta / (2 - $max - $min);

	$delta_r = ( ( ( $max - $r) / 6) + ($max / 2)  ) / $delta;
	$delta_g = ( ( ( $max - $g) / 6) + ($max / 2)  ) / $delta;
	$delta_b = ( ( ( $max - $b) / 6) + ($max / 2)  ) / $delta;

	if($r == $max){
	  $hue = $delta_b - $delta_g;
	} else if($g == $max){
	  $hue = (1 / 3) + $delta_r - $delta_b;
	} else if($b == $max){
	  $hue = (2 / 3) + $delta_g - $delta_r;
	}

	if($hue < 0){
	  $hue += 1;
	}

	if($hue > 1){
	  $hue -= 1;
	}

	}

	$hsl = array($hue, $sat, $luma); 

	return $hsl;

	}

	public function complement($color){

	if(is_array($color)){
		$hsl = Self::rgb2hsl($color);
	} else {
		$hsl = Self::rgb2hsl(Self::hex2rgb($color));
	}

	$h = $hsl[0];
	$s = $hsl[1];
	$l = $hsl[2];

	$h2 = $h + 0.5;

	if ($h2 > 1){
	 $h2 -= 1;
	}

	if ($s == 0){
	          $r = $l * 255;
	          $g = $l * 255;
	          $b = $l * 255;
	  }
	  else
	  {
	      if ($l < 0.5) {
	          $var_2 = $l * (1 + $s);
	      }
	      else
	      {
	          $var_2 = ($l + $s) - ($s * $l);
	      }

	      $var_1 = 2 * $l - $var_2;
	      $r = 255 * Self::hue2rgb($var_1,$var_2,$h2 + (1 / 3));
	      $g = 255 * Self::hue2rgb($var_1,$var_2,$h2);
	      $b = 255 * Self::hue2rgb($var_1,$var_2,$h2 - (1 / 3));

	  }

	$rhex = sprintf("%02X",round($r));
	$ghex = sprintf("%02X",round($g));
	$bhex = sprintf("%02X",round($b));

	return $rgbhex = $rhex.$ghex.$bhex;

	}


	public function hue2rgb($v1,$v2,$vh) {
	if ($vh < 0){
	  $vh += 1;
	}

	if ($vh > 1){
	  $vh -= 1;
	}

	if ((6 * $vh) < 1){
	  return ($v1 + ($v2 - $v1) * 6 * $vh);
	}

	if ((2 * $vh) < 1){
	  return ($v2);
	}

	if ((3 * $vh) < 2){
	  return ($v1 + ($v2 - $v1) * ((2 / 3 - $vh) * 6));
	}

		return ($v1);
	}



	public function hsl2rgb($hsl){

			$h = $hsl[0];
			$s = $hsl[1];
			$l = $hsl[2];

	        $r = $l;
	        $g = $l;
	        $b = $l;
	        $v = ($l <= 0.5) ? ($l * (1.0 + $s)) : ($l + $s - $l * $s);
	        if ($v > 0){
	              $m;
	              $sv;
	              $sextant;
	              $fract;
	              $vsf;
	              $mid1;
	              $mid2;

	              $m = $l + $l - $v;
	              $sv = ($v - $m ) / $v;
	              $h *= 6.0;
	              $sextant = floor($h);
	              $fract = $h - $sextant;
	              $vsf = $v * $sv * $fract;
	              $mid1 = $m + $vsf;
	              $mid2 = $v - $vsf;

	              switch ($sextant)
	              {
	                    case 0:
	                          $r = $v;
	                          $g = $mid1;
	                          $b = $m;
	                          break;
	                    case 1:
	                          $r = $mid2;
	                          $g = $v;
	                          $b = $m;
	                          break;
	                    case 2:
	                          $r = $m;
	                          $g = $v;
	                          $b = $mid1;
	                          break;
	                    case 3:
	                          $r = $m;
	                          $g = $mid2;
	                          $b = $v;
	                          break;
	                    case 4:
	                          $r = $mid1;
	                          $g = $m;
	                          $b = $v;
	                          break;
	                    case 5:
	                          $r = $v;
	                          $g = $m;
	                          $b = $mid2;
	                          break;
	              }
	        }
	        return array('r' => $r * 255.0, 'g' => $g * 255.0, 'b' => $b * 255.0);
	}

	public function rgb2hex($rgb){
		return '#' . sprintf('%02x', $rgb['r']) . sprintf('%02x', $rgb['g']) . sprintf('%02x', $rgb['b']);
	}

}
