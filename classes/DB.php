<?php

class DB {

	// Variable for DB instantiation
	private static $_instance = null;

	private $_pdo,
	 		$_error = false, 
	 		$_query,
	 		$_results,
	 		$_id = null,
	 		$_count = 0;


	private function __construct() {
		
		try{
			$this->_pdo = new PDO('mysql:host=' . Config::get('mysql/host') . ';dbname=' . Config::get('mysql/db'), Config::get('mysql/user'), Config::get('mysql/password') );
		} catch(PDOException $e) {
			echo 'Fatal Database Error';
			die( $e->getMessage() );
		}
	
	} // Close constructor

	public static function getInstance(){

		if(!isset(self::$_instance)){
			self::$_instance = new DB();
		} 

		return self::$_instance;

	}

	// Simple pdo querying

	public function query($sql, $parameters = array() ) {

		// Reset error handling
		$this->_error = false;

		if($this->_query = $this->_pdo->prepare($sql) ){

			if(count($parameters) && !empty($parameters)){

				// Bind parameters starts on 1
				
				$x = 1;
				foreach ($parameters as $parameter) {
					$this->_query->bindValue($x, $parameter);
					$x++;

				} //  end foreach
			} // end if count


			if( $this->_query->execute() ){

				$this->_id 		= $this->_pdo->lastInsertId();
				$this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
				$this->_count 	= $this->_query->rowCount();
			} else {
				$this->_error = true;
			}

		}  // end prepare check 

		return $this;

	}

	public function action($action, $table, $where = array() ){
		if( count($where) === 3 ){
			$operators = array('=', '>', '<', '>=', '<=');

			$field 		= $where[0];
			$operator 	= $where[1];
			$value 		= $where[2];

			if(in_array($operator, $operators)){
				
				// Format SQL query
				$sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";
				if( !$this->query($sql, array($value))->error() ){
					return $this;
				}
			}

		}

		return false;
	} // end Action function

	public function count(){
		return $this->_count;
	}

	public function id(){
		return $this->_id;
	}
	// Where takes form array('id', '=', '1')
	public function get($table, $where){
		return $this->action('SELECT *', $table, $where);
	}

	public function delete($table, $where){
		return $this->action('DELETE', $table, $where);
	}

	// Return results object
	public function results(){
		return $this->_results;
	}

	// Return only first result
	public function first(){
		return $this->results()[0];
	}

	public function error(){
		return $this->_error;
	}

	// Insert data

	public function insert($table, $fields = array()){
		if(count($fields)) {
			$keys 	= array_keys($fields);
			$values = null;
			$x 		= 1;

			foreach($fields as $field){
				$values .="?";
				if($x < count($fields)){
					$values .= ", ";
				}
				$x++;
			} 

				$sql = "INSERT INTO {$table} (`" . implode('`, `', $keys) . "`) VALUES ({$values})";
			
			if($this->query($sql, $fields)->error()){
				return true;
			}


		}
		return false;
	}


	// Update table Where array('id', $value)

	public function update($table, $where, $fields) {
		$set = "";
		$x = 1;
		foreach($fields as $name => $value){
			$set .= "{$name} = ?";
			if($x< count($fields)) {
				$set .= ", ";
			}
			$x++;
		}

		$sql = "UPDATE `{$table}` SET {$set}  WHERE `{$where[0]}` = {$where[1]}";


			if(!$this->query($sql, $fields)->error()){
				return true;
			}
			return false;
	}


	public function search($table, $term, $columns, $where = array()) {

		$columns = (array) $columns;


		$sql = "SELECT * FROM {$table} WHERE {$columns[0]} LIKE '%{$term}%'";
		
		if(count($columns) > 1){

			unset($columns[0]);

			foreach ($columns as $index => $column) {
				$sql .= " OR {$column} LIKE '%{$term}%' ";
			}
		}

		if(!empty($where)){
			$sql .= " AND ".$where[0]."= '".$where[1]. "'";	
		}
		

		if(!$this->query($sql)->error()){
			return true;
		}

		return;

	}





}
