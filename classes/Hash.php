<?php

class Hash {

	public static function make($length = 62){
		return substr(bin2hex(openssl_random_pseudo_bytes($length)), 0, $length);
	}

	public function password($password){
		return password_hash($password, Config::get('passwords/standard'), array('cost' => Config::get('passwords/cost')));
	}

}