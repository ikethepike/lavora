<?php include 'inc/head.php'; ?>

</head>
<body>


<?php 

if(!$user->isLoggedIn()){

	include 'inc/homepage.php';

} else {
	include 'inc/userbar.php';
	include 'inc/project_view.php';
} 

include 'inc/footer.php'; 

?>