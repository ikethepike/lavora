// Project Settings

var dir = ".";


// Date and Time

var currentdate = new Date();
var datetime = "Last Sync: " + currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/"
                + currentdate.getFullYear() + " @ "
                + currentdate.getHours() + ":"
                + currentdate.getMinutes() + ":"
                + currentdate.getSeconds();

// Dependencies

/* Install All
    npm install node-sass gulp gulp-uglify critical gulp-responsive isarray gulp-autoprefixer gulp-sass gulp-util gulp-pngmin gulp-svg2png gulp-size gulp-notify gulp-imagemin imagemin-pngquant gulp-livereload gulp-git gulp-plumber --save-dev
*/

    var gulp = require('gulp'),
    uglify = require('gulp-uglify')
    autoprefixer = require('gulp-autoprefixer')
    sass = require('gulp-sass')
    gutil = require('gulp-util')
    svg2png = require('gulp-svg2png')
    livereload = require('gulp-livereload')
    git = require('gulp-git')
    critical = require('critical')
    responsive = require('gulp-responsive')
    php2html = require("gulp-php2html")
    plumber = require('gulp-plumber');


//JS uglify
    gulp.task('compress', function() {
      return gulp.src(dir + '/js/*.js')
        .pipe(plumber())
        .pipe(uglify())
        .pipe(plumber.stop())
        .pipe(gulp.dest(dir + '/js/min/'))
    });

// CSS Autoprefixer
gulp.task('autoprefixer', function () {
    return gulp.src(dir + '/css/*.css')
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
    .pipe(gulp.dest(dir + '/css/'));
});

// Convert index to html for above the fold critical css
gulp.task('php2html', function(){
    gulp.src("index.php")
      .pipe(php2html())
      .pipe(gulp.dest(dir+"/static/"));
});


// Image optimization

gulp.task('img_sizes', function () {
  return gulp.src('src/*.{jpg,png}')
    .pipe(responsive({
      // Resize all JPG images to three different sizes: 200, 500, and 630 pixels
      '*.jpg': [{
        width: 200,
        rename: { suffix: '-200px' },
      }, {
        width: 500,
        rename: { suffix: '-500px' },
      }, {
        width: 630,
        rename: { suffix: '-630px' },
      }],
      // Resize all PNG images to be retina ready
      '*.png': [{
        width: 250,
      }, {
        width: 250 * 2,
        rename: { suffix: '@2x' },
      }],
    }, {
      // Global configuration for all images
      // The output quality for JPEG, WebP and TIFF output formats
      quality: 70,
      // Use progressive (interlace) scan for JPEG and PNG output
      progressive: true,
      // Strip all metadata
      withMetadata: false,
    }))
    .pipe(gulp.dest(dir + '/unoptimized/sizes/'));
});

gulp.task('critical', function (cb) {
  critical.generate({
    base: dir,
    src: 'static/index.html',
    css: 'css/base.css',
    dimensions: [{
      width: 320,
      height: 480
    },{
      width: 768,
      height: 1024
    },{
      width: 1280,
      height: 960
    }],
    dest: 'css/critical.html',
    minify: true,
    extract: false,
    ignore: ['font-face']
  });
});


//SCSS Processing
gulp.task('sass', function () {
    gulp.src(dir + '/sass/**/*.scss')
        .pipe(plumber())
        .pipe(sass().on('error', sass.logError))
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(plumber.stop())
        .pipe(autoprefixer())
        .pipe(gulp.dest(dir + '/css'));
        console.log('SASS compile done');
});

// SVG to PNG
gulp.task('svg2png', function () {
    gulp.src(dir+'/img/**/*.svg')
        .pipe(svg2png())
        .pipe(gulp.dest(dir + '/img/fallback/'));
});

// Git

gulp.task('add', function(){
  return gulp.src('.')
    .pipe(git.add());
});

gulp.task('commit', function(){
  return gulp.src('*')
    .pipe(git.commit(['build '+ datetime, 'Automatic build compilation']));
});

gulp.task('push', function(){
  git.push('origin', 'master', function (err) {
    if (err) throw err;
  });
});

gulp.task('pull', function(){
  git.pull('origin', 'master', {args: '--rebase'}, function (err) {
    if (err) throw err;
  });
});


// Watch Task
gulp.task('watch', function(){
    gulp.watch(dir+'/js/*.js', ['compress']);
    gulp.watch(dir+'/sass/**/*.scss', ['sass']);
    gulp.watch('*.php', ['php2html']);
    gulp.watch(dir+'/static/index.html', ['critical']);

});

gulp.task('live', function(){
    livereload.listen();
    gulp.watch(dir+'/sass/**/*.scss', ['sass']);
    gulp.watch(dir+'/css/**/*.css', ['autoprefixer']);
});


gulp.task('build', function(){
    gulp.start('compress', 'sass', 'autoprefixer', 'img', 'add', 'commit', 'push');

});

gulp.task('default', function() {
    gulp.start('watch');
});