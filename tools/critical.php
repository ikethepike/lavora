<?php 

require_once('../core/init.php');


// Get the index file
$request = new Request;
$request->get(HOME_URL . '/index.php'); 
$index = $request->response() ;


// Link to critical html target
$critical =  '../css/critical.html';


if(!file_exists($critical)){
	$critical = fopen($critical, 'w');
} else {
	$critical = fopen($critical, 'r'); 
}

if(fwrite($critical, $index)){
	return 'success';
} 
return 'error';
