<div id='login-form'>
	<form method="POST">
		<div class='field' id='username-wrapper'>
			<label for='login-username'> Username </label>
			<input type='text' name='login-username' id='login-username' placeholder='Username'>
		</div>

		<div class='field' id='password-wrapper'>
			<label for='login-password'> Password </label>
			<input type='password' name='login-password' id='login-password' placeholder='Password'>
		</div>

		<div class="field" id='checkbox-wrapper'>
			<label for='remember'>
				<input type='checkbox' name="remember" id='remember'>
				Remember me?
			</label>
		</div>

		<div class='submit-wrapper'>
			<input type='hidden' name='login-token' value='<?php echo Token::generate(); ?>'>
			<input type='submit' class='submit-button' value='Log in'>
		</div>

	</form>
</div>