<div id="uploads"></div>

<?php $edit = false; ?>

<div class="dropzone" data-hash='<?php echo $temp = uniqid(); ?>' id="dropzone" style='<?php echo ($edit) ? "background: url($feat_image) no-repeat center; background-size: cover;" : Input::get('image-link'); ?>'>

	<span> Drag files here to upload </span>
	
	<div id='dropzone-dim-<?php echo $temp; ?>' class="dropzone-dim'"></div>
	
	<!-- Random hash correlated against DB to set featured image, image link solely used to maintain placeholder img -->
	<input type="hidden" name="image-hash" class='image-hash' id="image-hash-<?php echo $temp; ?>" value='<?php echo Input::get('image-hash'); ?>'/>
	<input type="hidden" name='image-link' class='image-link' id='image-link-<?php echo $temp; ?>' value='<?php echo Input::get('image-link'); ?>'>

</div>

