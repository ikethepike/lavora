
<?php

		if(Input::exists() && !empty($_POST)){

			if(Token::check(Input::get('register-token')) ) {				

				$validate = new Validate();
				$validation = $validate->check($_POST, array(
					'username'			=> array(
						'item'		=> 'Username',
						'table'		=> 'users',
						'required'	=> true,
						'min'		=> 3,
						'max'		=> 30,
						'alpha'		=> true,
						'unique'	=> true
					),
					'password'			=> array(
						'item'		=> 'Password',
						'required'	=> true,
						'min'		=> 6,
						'max'		=> 50,
						'unique'	=> false
					),
					'password_repeat'	=> array(
						'item'		=> 'Password',
						'required'	=> true,
						'min'		=> 6,
						'max'		=> 50,
						'matches'	=> 'password'
					),
					'email'				=> array(
						'item'		=> 'Email',
						'table'		=> 'users',
						'unique'	=> true,
						'required'	=> true,
						'min'		=> 5,
						'max'		=> 32
					)
				));

				if($validation->passed()) {

					$hash = new Hash;
					$user = new User();

					$password_hash	= $hash->password(Input::get('password'));
					$activation 	= input::get('username') + uniqid();
					$activation		= md5( $activation );

					$color = new Color();


					$DB = DB::getInstance();

					$DB->insert('users', array(
							'username'		=> Input::get('username'),
							'password'		=> $password_hash,
							'activation'	=> $activation,
							'email'			=> Input::get('email'),
							'first_name' 	=> Input::get('first_name'),
							'surname'		=> Input::get('surname'),
							'dob'			=> Input::get('dob'),
							'color'			=> $color->random()
						)
					);

					if($DB->error()){
						die("Error creating user");
					}

					$user->login(Input::get('username'), Input::get('password') );

					Session::flash('success', $DB->id() );

					Redirect::to(HOME_URL . '/intro');

				} else {
					$number = count($validation->errors());
					echo "<div class='site-message-wrapper errors-{$number}'>";
					foreach ($validation->errors() as $error ) {
						echo  $error;
					}
					echo "</div>";
				}

			} // Token Check

		} // Input exists

?>


<div id='register-form-wrapper'>
	<form action='' method="POST">
		<div class='field'>
			<label for='username'> Username </label>
			<input type='text' name='username' id='username' value='<?php echo Input::get("username"); ?>'>
		</div>

		<div class='field'>
			<label for='email'> Email </label>
			<input type='email' name='email' id='email' value='<?php echo Input::get("email"); ?>'>
		</div>

		<div class="field">
			<label for='password'> Password </label>
			<input type='password' name='password' id='password'>
		</div>

		<div class="field">
			<label for='password_repeat'> Confirm Password </label>
			<input type='password' name='password_repeat' value='' id='password_repeat'>
		</div>

		<div class="field">
			<label for='first_name'> First Name </label>
			<input type='text' name='first_name' value='<?php echo Input::get('first_name'); ?>' id='first_name'>
		</div>

		<div class="field">
			<label for='surname'> Surname </label>
			<input type='text' name='surname' value='<?php echo Input::get('surname'); ?>' id='surname'>
		</div>

		<div class="field">
			<label for='dob'> Date of Birth </label>
			<input type='date' name='dob' value='<?php echo Input::get('dob'); ?>' id='dob'>
		</div>

		<div class='submit-wrapper'>
			<input type='hidden' name='register-token' value='<?php echo Token::generate(); ?>'>
			<input type="submit" class='submit-button' id='register-button' value='Register'>
		</div>

	</form>
</div>