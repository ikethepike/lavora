<?php 

	$color = new Color;

?>



<div class="color-picker">
	<input type="hidden" name="color-choice" id="color-choice">

	<div class="palette">
		<?php 
			$activeColor = '#fe495a';
			for($i = 0; $i < 20; $i++)
			{

				if(is_integer($i / 5) && $i !== 0){
					$activeColor = $color->random();
				}

				echo "<div data-color='{$activeColor}' class='color' style='background-color: {$activeColor};'></div>";
				$activeColor = $color->shift($activeColor , 87, 'hex');

			} 
		?>

	</div>

</div>

<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(){
		var colors 		= document.getElementsByClassName('color'),
			colorChoice = document.getElementById('color-choice');

			for (var i = 0; i < colors.length; i++) {
				colors[i].addEventListener('click', function(){
					unsetColors();

					this.className += ' chosen-color';
					colorChoice.value = this.getAttribute('data-color');


				});
			}


			function unsetColors(){
				for (var i = 0; i < colors.length; i++) {
					colors[i].className = 'color';
				}
			}


	});
</script>