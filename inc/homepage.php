<header id="home-header">
	<nav>
		<div id="main-nav-wrapper">
			<div class="nav-links">
				
			</div>
			<div id="logo-wrapper">
				<?php include 'img/dodo.svg'; ?>
			</div>
			<div class="nav-links">
				<ul>
					<li>
						<a href="<?php echo HOME_URL; ?>/login">Login</a>
					</li>
					<li>
						<a href="<?php echo HOME_URL; ?>/register">Register</a>
						
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>

<main>
	<header id="home-masthead">
		
		<div id="intro-block">
			<h1><?php echo $site_title; ?> helps you to manage projects and client communication.</h1>
		</div>		

	</header>
</main>