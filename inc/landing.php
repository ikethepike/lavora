
<div id='welcome-home'>
	
	<div id='welcome-content-wrapper'>
		
		<header id="welcome-header">
			<h2>Projects</h2>

			<a href='<?php echo HOME_URL; ?>/create' id='zippy-link' data-link="project-creation-wrapper">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 width="500px" height="500px" viewBox="0 0 500 500" enable-background="new 0 0 500 500" xml:space="preserve">
				<line fill="none" stroke="#000000" stroke-width="60" stroke-miterlimit="10" x1="250" y1="0" x2="250" y2="500"/>
				<line fill="none" stroke="#000000" stroke-width="60" stroke-miterlimit="10" x1="0" y1="250" x2="500" y2="250"/>
				</svg>
			</a>

		</header>
	
		<div id='welcome-projects-wrapper' class="projects-wrapper">
		<?php 

			$projects = json_decode($user->data()->projects);

			if(!empty($projects)):
				$DB = DB::getInstance();
				foreach ($projects as $id) : 
					
					$DB->get('projects', array('id', '=', $id));
					$project 	= $DB->first(); 


					$DB->get('tasks', array('project_id', '=', $id));
					$tasks 		= $DB->results();

					if(isset($tasks)){
						$total_tasks 	= count((array) $tasks);
						$collection 	= new Collection($tasks);
						$not_started 	= false; 
						$completed 		= $collection->search('status', "2");

						if($total_tasks !== 0){
							$percent = round((count($completed) / $total_tasks) * 100); 
						} else {
							$not_started = true;
						}


					}

					?>
				
				<!-- Fix linking -->

				<div class='project-tile'> 
					<a href="<?php echo "?id={$project->id}"; ?>">
					<div class='project-poster' style="background: #f2f2f2 url(<?php echo $project->feat_img; ?>) no-repeat center; background-size:cover;">
						
						<div class='project-status <?php echo ($not_started) ? "not-started" : "started"; ?>'>
							
							<?php echo ($not_started) ? "Not started" : $percent . "%"; ?>
			
						</div>

					</div>
										
					<div class='project-tile-button' role='button'>
						<h3><?php echo $project->name; ?> </h3>
						<div class="arrow"></div>
					</div>

					</a>
				</div>


				<?php endforeach; 
			endif; ?>

		</div> <!-- Projects Wrapper -->

	</div> <!-- Content Block -->
</div>