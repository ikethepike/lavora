<?php
	// General Site Vars and functions
	
	$reqs = get_included_files();

	if(!in_array( realpath(__DIR__ . '\..') . '\core\init.php', $reqs)){
		require "core/init.php";
	}
	
	$user = new User();  


?>

<!DOCTYPE html>
<html lang='en'>
<head>

	<!-- Single page site 
	<title> <?php // echo $page_title; ?> </title>
	-->

	<!-- Multi page site -->
	<title> <?php echo $site_title; if(isset($subpage_title)){ echo " | " . $subpage_title; } ?> </title>


	<?php 
	$media = 'screen';
	if(!isset($_COOKIE['dodoDone'])): 
	$media = 'async';
	?>
	
	<script defer type="text/javascript" src='js/min/load.js'></script>
	
	<style>
		<?php include 'css/critical.html'; ?>
	</style>

	<?php endif; ?>
	
	<link rel="stylesheet" id='main-stylesheet' type="text/css" media='<?php echo $media; ?>' href="<?php echo HOME_URL; ?>/css/base.css">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<script defer type="text/javascript">
	!function(e,t,n,a,c,l,m,o,d,f,h,i){c[l]&&(d=e.createElement(t),d[n]=c[l],e[a]("head")[0].appendChild(d),e.documentElement.className+=" wf-cached"),function s(){for(d=e[a](t),f="",h=0;h<d.length;h++)i=d[h][n],i.match(m)&&(f+=i);f&&(c[l]="/**/"+f),setTimeout(s,o+=o)}()}(document,"style","innerHTML","getElementsByTagName",localStorage,"tk",/^@font|^\.tk-/,100);
		
	</script>
	<script defer>

	  (function(d) {
	    var config = {
	      kitId: 'wek6qny',
	      scriptTimeout: 3000,
	      async: true
	    },
	    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
	  })(document);
	</script>

	<script type="text/javascript" defer src='js/min/scripts.js'></script>
	<script type="text/javascript" defer src='js/sortable.js'></script>
	