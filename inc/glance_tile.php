<li class='glance-tile' data-id='<?php echo $task->id; ?>' data-hash="<?php echo $task->hash; ?>">
	<a href="task?id=<?php echo $task->id; ?>" data-hash="<?php echo $task->hash; ?>" class="task-popup open-link" data-link='' data-id="<?php echo $task->id; ?>">
		<div class="text-wrapper">
			<span class="date"><?php echo $date;  ?></span>
			<span class="name"><?php echo $task->name; ?></span>
		</div>
		<ul class="task-owners">
			<?php
				$owners[] = json_decode($task->owner); 

				foreach ($owners as $index => $owner) {
					$member = new User($owner);
					unset($owners[$index]);
					include 'member_orb.php'; 
				} ?>
		</ul>
	</a>
</li>