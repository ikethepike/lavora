<div class='project-pane'>

<?php

// Check if ID set, else landing page 
if(isset($_GET['id'])):

include 'inc/header.php';

$DB = DB::getInstance();

$project_id = $_GET['id'];

$_SESSION['project_id']	= $project_id;
$_SESSION['base_url']  	= BASE_URL;


// Instantiate the project by id
$DB->get('projects', array('id', '=', $project_id));
$project = $DB->first();

// Get all associated tasks
$DB->get('tasks', array('project_id', '=', $project_id));
$tasks = $DB->results();
$total_tasks = count($tasks);

$task_collection = new Collection($tasks);

$completed_tasks = $task_collection->search('status', '2');

if($total_tasks == 0){
	$percent = 0;
} else {
	$percent = round((count($completed_tasks) / $total_tasks ) * 100);
}

$team = json_decode($project->owners);

foreach($team as $member){
	$DB->get('users', array('id', '=', $member));
	$members[$member] = $DB->first();
}

?>


<header class="project-header" style='background: url(<?php echo $project->feat_img; ?>) no-repeat center; background-size: cover;'>
	<a title='Projects' href='<?php echo HOME_URL; ?>'>
		<div class="project-toggle overview-toggle">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="500px" height="500px" viewBox="0 0 500 500" enable-background="new 0 0 500 500" xml:space="preserve">
				<circle cx="82.853" cy="83.308" r="50.102"></circle>
				<circle cx="250" cy="83.308" r="50.102"></circle>
				<circle cx="416.751" cy="83.308" r="50.102"></circle>
				<circle cx="82.719" cy="250.3" r="50.102"></circle>
				<circle cx="250" cy="250" r="50.102"></circle>
				<circle cx="416.751" cy="250.5" r="50.102"></circle>
				<circle cx="82.718" cy="416.643" r="50.102"></circle>
				<circle cx="250" cy="416.642" r="50.102"></circle>
				<circle cx="416.751" cy="416.642" r="50.103"></circle>
			</svg>
		</div>
	</a>
	
	<header class="project-heading">
		<h1><?php echo $project->name; ?></h1>
		<?php if(!empty($tasks)): ?>
		<progress class="project-progress" value='<?php echo $percent; ?>' max='100'></progress>
		<?php endif; ?>
	</header>
	

	<footer class="project-stats">
		<ul>
			<li>
				<span class='text'> Budget: </span> <span class="value budget"> <?php echo $project->budget; ?><span class='currency'> <?php echo $project->currency; ?> </span> </span> 
			</li>
			<li>
				<span class='text'> Project Completion: </span> <span class="value"> <?php echo $project->etc; ?> </span>
			</li>
		</ul>

		<div id='team-list'>
			<?php 
				$total = count($team);
				$width  = 100 / $total;
				foreach($team as $member){
					$member = new User($member);

					$team_members[] = $member->data();

					echo "<a href='profile?user_id={$member->data()->id}' class='member' style='background: {$member->data()->color}; width: {$width}%;'>";
					echo substr($member->data()->first_name, 0, 1) . substr($member->data()->surname, 0, 1); 
					echo "</a>";
				}
			?>
		</div>



	</footer>

	<div class="dim"></div>
</header>



<div class="content-pane">

	
	<?php

	include 'inc/powerbar.php';
	include 'inc/flow.php';
	include 'inc/my_sidebar.php';


	?>
	
</div>


<?php else: // Active project check ?>


<?php include BASE_URL . "inc/landing.php"; ?>


<?php endif; ?>

</div> <!-- End of project pane -->

