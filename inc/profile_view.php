<?php 
	


	if(isset($_GET['user_id'])){
		$member = new User($_GET['user_id']); 
	} else {
		$member = new User($user->data()->id);
	}
	
	$fullName = $member->data()->first_name . " " . $member->data()->surname; 
	$role 	  = ""; // Add me

?>



<form method="POST">
	<header id="profile-hero">
		<?php include BASE_URL . '/modules/file-upload.php'; ?>
		<div class="dim"></div>
	</header>
</form>



<?php if(isset($_GET['changeAvatar'])){
	include "inc/change_avatar.php";
	echo "<div class='page-dim-active'></div>";
} ?>

<div id="profile-wrapper">
	<div id="bio-block">
		<div id="main-avatar-wrapper" style='background: <?php echo $member->data()->color; ?> url(<?php echo $member->data()->profile_pic; ?>) no-repeat center;background-size:cover;'>
				<a href="?user_id=<?php echo $member->data()->id;?>&changeAvatar" id="change-image"></a>
		</div>

		<div class="body-wrapper">
			<div class="title-block">
				<aside class="quick-bio">
					<h2 class='name'><?php echo "<span class='first'> {$member->data()->first_name} </span> {$member->data()->surname}"; ?></h2>
					<h3 class='role'><?php echo $role; ?> </h3>
				</aside>
			</div>

			<div class="actions-block">
				
				<?php if($user->data()->id != $member->data()->id): ?>

					<a class="button" data-token='<?php echo Token::generate(); ?>' id="add-connection">
						<?php if($user->friends($member->data()->id)): ?>
							Remove Connection
						<?php else: ?>
							Add Connection
						<?php endif; ?>
					</a>
					
					<a href="" class="button">
						Message 
					</a>
				<?php endif; ?>

			</div><!--  Actions Block -->
		</div> <!-- Body wrapper -->
		

	</div> <!-- Bio block end -->



</div>


<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(){

			var connectionButton = document.getElementById('add-connection');

			connectionButton.addEventListener('click', function(){

					token = connectionButton.getAttribute('data-token');

					var data = {
						id : <?php echo $member->data()->id; ?>,
						token: token,
					}

					post('handlers/connection.php', data, function(responseText){

							var response = JSON.parse(responseText);

							if(response.status !== 1){
								alert('Your request timed out, please refresh the page and try again.');
								return;
							}
							// Style button 
							if(response.operation == 'added'){
								connectionButton.classname = 'button friends';
								connectionButton.innerHTML = "Remove connection";
							} else {
								connectionButton.classname = 'button not-friends';
								connectionButton.innerHTML = "Add connection";
							}


					});
			});


	});



</script>