<?php 


$completion = strtotime($project->etc);
$startTime 	= strtotime($project->start);

$days = round(abs($startTime - $completion)/60/60/24);

?>
<header id="planner-header">
	<h1 id="days-left"><span id='days'><?php echo $days; ?><span class='text'> days left </span></h1>
</header>

<div id="timeline-wrapper">
	<div id="timebar"></div>
</div>


<div id="timeline-body">
	
</div>
