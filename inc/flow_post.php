<?php if(isset($_SESSION['task_id'])){
	
	$task = $DB->get('tasks', array('id', '=', $_SESSION['task_id']))->first(); 
	unset($_SESSION['task_id']);

} ?>


<li class="flow-post <?php if(!empty($task->feat_img)){echo 'flow-post-img' ;} ?>">
	<?php if(!empty($task->feat_img)): ?>
		
		<div class="thumbnail">	
			<img src="<?php echo $task->feat_img; ?>" alt="">
		</div>

	<?php endif; ?>

	<div class="body-wrapper">	
		<div class="top-wrap">
			
			<a href="task.php?id=<?php echo $task->id; ?>" class='ajax-link' data-link='views/task_view.php?id=<?php echo $task->id; ?>'>
				<h2 class="name"><?php echo $task->name; ?></h2>
				<span class="date"><?php echo $task->due; ?></span>
			</a>

		</div>
		
		<ul class="owners">
			<?php 
				$output = array();
				$owners = json_decode($task->owner);
				if(is_array($owners)){
					foreach ($owners as $index => $owner) {
						$output[] = $owner;
					}
				} else {
					$output[] = $owners;
				}

				foreach ($output as $task_owner) {
					$member = new User($task_owner); 

					include 'member_orb.php';


				}	?>
		</ul>


	</div>
</li>