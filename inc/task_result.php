<div class="result-tile task-result">
	<a href="task.php?id=<?php echo $task->id; ?>" class='ajax-link' data-link='views/task_view.php?id=<?php echo $task->id; ?>'>

	<?php if(!empty($task->feat_img)): ?>
		<header class="task-thumbnail" style="background: url(<?php echo $task->feat_img; ?>) no-repeat center; background-size: cover;"></header>	
	<?php endif; ?>
	
	<div class="text-block">
		<h3><?php echo $task->name; ?></h3>
		<p class="text"><?php echo substr($task->description, 0, 180); ?></p>
	</div>
	</a>
</div>