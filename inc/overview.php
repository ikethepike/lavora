<div id="project-overview" class='project-overview'>
	<header>
		<h2>Choose a project or create a new one</h2>
		<div id='close-overview' class="overview-toggle">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="500px" height="500px" viewBox="0 0 500 500" enable-background="new 0 0 500 500" xml:space="preserve">
			<path fill="none" stroke="#000000" stroke-width="100" stroke-miterlimit="10" d="M45,45l410,410 M455,45L45,455"/>
			</svg>

		</div>
	</header>
	<div class="projects-wrapper">
		<ul>

			<?php 

				if(!isset($user)){
					$user = new User;
				}

				$projects = json_decode($user->data()->projects);

				if(!empty($projects)):
					$DB = DB::getInstance();
					foreach ($projects as $id) : 
						
						$DB->get('projects', array('id', '=', $id));
						$project 	= $DB->first(); 


						$DB->get('tasks', array('project_id', '=', $id));
						$tasks 		= $DB->results();

						if(isset($tasks)){
							$total_tasks 	= count((array) $tasks);
							$collection 	= new Collection($tasks);
							$not_started 	= false; 
							$completed 		= $collection->search('status', "2");

							if($total_tasks !== 0){
								$percent = round((count($completed) / $total_tasks) * 100); 
							} else {
								$not_started = true;
							}


						}

						?>
								


					<li class='project-tile'> 
						<a href="<?php echo "?id={$project->id}"; ?>">
						<div class='project-poster' style="background: #f2f2f2 url(<?php echo $project->feat_img; ?>) no-repeat center; background-size:cover;">
							
							<div class='project-status <?php echo ($not_started) ? "not-started" : "started"; ?>'>
								
								<?php echo ($not_started) ? "Not started" : $percent . "%"; ?>
					
							</div>

						</div>
											
						<div class='project-tile-button' role='button'>
							<h3><?php echo $project->name; ?> </h3>
							<div class="arrow"></div>
						</div>

						</a>
					</li>


					<?php endforeach; 
				endif; ?>







		</ul>
	</div>
</div>