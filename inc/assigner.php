<?php 

$_POST['project_id'] = 15;

if(!empty($_POST) && isset($_POST['project_id'])){
	$project_id = $_POST['project_id'];

	if(!isset($DB)){
		require_once 'core/init.php';
		$DB = DB::getInstance();
	}

	$DB->get('projects', array('id', '=', $project_id));

	$project = $DB->first();

	$owners  = array();
	foreach(json_decode($project->owners) as $index => $owner){
		$DB->get('users', array('id', '=', $owner));
		$owners[] = $DB->first();
	}
} 

?>

<div id="assigner">
	<header>
		<h4>Assign to:</h4>
	</header>
	<ul>
		<?php foreach($owners as $owner){ ?>
			<li class='assigner-member' data-id='<?php echo $owner->id; ?>'>
				<div class='plus'>
					+
				</div>
				<div class='name-wrapper'> 
					<span class='name'> <?php echo $owner->first_name . " " . $owner->surname; ?> </span>
				</div>
			</li>
		<?php } ?>
	</ul>
</div>
