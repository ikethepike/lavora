<script type="text/javascript" src='js/handler.js'></script>

<div id="project-creation-wrapper" class='modal'>
	<div class="wrap">
		<section class="creating-project">
			<form id='creation-form'>
				<header id="project-image">
					<?php include 'modules/file-upload.php'; ?>
				</header>

				<div id="project-title">
					<input type="text" name="project-title" id='project-name' placeholder='Project Name' value='<?php echo Input::get('project-title'); ?>'>
					<div id='category-wrapper'>
						<select name="project-category" id="project-category">
							<option value="Design"> Design</option>
							<option value="Development"> Development</option>
						</select>
					</div>
				</div>

				<div class="block-wrapper">
					<div class="half">
						<label for="description">Describe your project </label>
						<textarea name="description" id="project-description" cols="30" rows="10"></textarea>
					</div>

					<div class="half">
						<div class="field">
							<label for="budget"> Project budget </label>
							<input type="number" name='budget' id='project-budget' value='0' min='0' step='50'>
						</div>

						<div class="field">
							<label for="etc">Estimated completion date</label>
							<input type="date" name='etc' id='etc'>
						</div>

					</div>

				</div>
				
				<div class="submit-wrapper">
					<input type="submit" value="Create">
				</div>
			</form>
		</section>
	</div> <!-- End of Wrap -->
</div>

<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function(ev) {

	var form = document.getElementById('creation-form');

	form.addEventListener('submit', function(e){
		e.preventDefault();

		var title 		= document.getElementById('project-name').value,
			category 	= document.getElementById('project-category').value,
			budget 		= document.getElementById('project-budget').value,
			etc 		= document.getElementById('etc').value,
			description = document.getElementById('project-description').value,
			feat_img	= form.querySelector(".image-hash").value;


			if(feat_img !== null){
				feat_img = '';
			}

			var data = {
				'contents': {
					'name' 			: title,
					'description' 	: description,
					'budget'		: budget,
					'category'		: category,
					'feat_img'		: feat_img,
					'etc'			: etc,
				}
			}




				project(data, function(response){
					console.log(response);
				if(response !== false){
					location.replace(window.location.href + "?id=" + response);
				} else {
					alert('Error creating project');
				}
			});


	});

});





</script>