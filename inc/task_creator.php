<div id="task-creator" class='modal collapsed-task-creator'>
	<form action="" id='task-form'>
		<header id="task-image">
			<?php include 'modules/file-upload.php'; ?>
		</header>
		<section class="main">
			<div class='input-wrapper'>
				<div class='field-wrapper'>
					<label for="name">Task name </label> 
					<input type="text" class='input-field' name='task-name' id="task-name">
				</div>

				<div class="textarea-wrapper">
					<label for='task-description'> Task description </label>
					<textarea name="task-description" class='textarea-field' id="task-description" cols="30" rows="10"></textarea>
				</div>

				<div class="field-wrapper">
				<label for="assign-owner">Assign to</label>
				<select class='select-field' name="assign-owner" id="assign-owner">
					<option value="null">Choose</option>
					<?php 
						$color = new Color();
						foreach($team_members as $index => $member){
							echo "<option value='{$member->id}'> {$member->first_name} {$member->surname} </option>";
						}
					?>
				</select>
				</div>
			</div>
		</section>
		
		<section class="extra">
			<header id='extra-header'>
				<h3>More Details</h3>
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 width="250px" height="500px" viewBox="0 0 250 500" enable-background="new 0 0 250 500" xml:space="preserve">
				<polyline points="0,0 250,250 0,500 "/>
				</svg>

			</header>
			<div id="task-creator-extra">

				<div class='input-wrapper'>
					<div class='field-wrapper'>
						<label for="due-date">Due date</label>
						<input class='input-field' type="date" min="<?php echo date('Y-m-d'); ?>" max="<?php echo $project->etc; ?>" name="task-due-date" id="task-due-date">
					</div>
					
					<div class='field-wrapper'>
						<label for="priority">Priority</label>
						<select class='select-field' name="task-priority" id="task-priority">
							<option value="High">High</option>
							<option value="Normal">Normal</option>
							<option value="Low">Low</option>
						</select>
					</div>
				</div>
			</div>	
		</section>
		<div class="submit-wrapper">
			<input type="submit" class='bottom-button' value="Add task">
		</div>
	</form>
</div>