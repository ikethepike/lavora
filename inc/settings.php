<div id="settings" class='modal open-modal ajax-modal'>
	<header class="modal-header">
		<ul>
			<li></li>
			<li class='title'>Settings</li>
			<li id='settings-close' class='close-link close-button'>
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 width="500px" height="500px" viewBox="0 0 500 500" enable-background="new 0 0 500 500" xml:space="preserve">
				<path fill="none" stroke="#000000" stroke-width="60" stroke-miterlimit="10" d="M45,45l410,410 M455,45L45,455"/>
				</svg>
			</li>
		</ul>
	</header>

	<div class="wrap">
		<div id="side-column">
			<ul>
				<li class='settings-link' data-setting='project_settings'>Project Settings</li>
				<li class='settings-link' data-setting='team_management'>Team Management </li>
				<li class='settings-link' data-setting='delete_project'>Delete Project	</li>
				<li class='settings-link' data-setting='about_lavora'>About Lavora </li>
			</ul>
		</div>
		
		<div id="main">
			<div id="settings-ajax"></div>
		</div>
	</div>

</div>