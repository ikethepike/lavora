
<div id="task-view">
	<ul>	
		<?php 
			if(!isset($DB)){
				require_once('../core/init.php');
				$DB 		= DB::getInstance();
				$project_id = $_SESSION['project_id'];
			}

			$DB->get('tasks', array('project_id', '=', $project_id));
			$tasks = (array) $DB->results();

			// Sort tasks by status
			usort($tasks, function($a, $b) {
			    return $a->status - $b->status;
			});



			$collection = new Collection($tasks);
			$task_count = 0;
			$finished_tasks = array();

			// Instantiate the project by id
			$DB->get('projects', array('id', '=', $project_id));
			$project = $DB->first();

			$team = json_decode($project->owners);

			$user = new User();

			if(!$collection->isEmpty()){

				foreach ($team as $index => $member_id){



					$user_tasks[$member_id] = $collection->search('owner', $member_id, $tasks);


					foreach ($user_tasks[$member_id] as $key => $value) {


						if($tasks[$key]->status == 2){
							$finished_tasks[] = $value;

						}
					}


					if(count($user_tasks[$member_id]) > $task_count){
						$task_count = count($user_tasks[$member_id]);
					}
					
				}

				echo "<div id='task-list' class='tasks-{$task_count}'> <ul class='task-count' id='test-1'>";
					for($i = 1; $i <= $task_count; $i++){
						echo "<li class='count'>{$i}</li>";
					}
					echo "</ul>"; // End of task-count

					foreach ($team as $index => $member_id){ 

						echo "<div class='user-section' id='user-{$member_id}'>";

						$member = new User($member_id);
						?>


							<header class='avatar-block'>
								<?php include BASE_URL . "inc/member_orb.php"; ?>
							</header>


						<?php
						if(!empty($user_tasks[$member_id])){
							echo "<ul class='user-tasks'>";
							foreach ($user_tasks[$member_id] as $key => $value) {

								$task = (array) $tasks[$key];

								switch ($task['status']) {
									case 0:
										$status = 'not-started';
										break;
									case 1:
										$status = 'in-progress';
										break;
									case 2:
										$status = 'finished';
										break;
									case 3:
										$status = 'late';
										break;
								}

								echo "<li class='task {$status}'><div class='handle'></div></li>";


							}
							echo "</ul>";
						}

						echo "</div>"; // end of user-section

					}

				echo "</div>";




			} else {
				echo "empty";
			}


		?>

	</ul>	
	</div>

	<script type="text/javascript" src='js/sortable.js'></script>

	<script type="text/javascript">
		document.addEventListener('DOMContentLoaded', function(){

			var taskLists = document.getElementsByClassName('user-tasks');

			for(var i = 0; i < taskLists.length; i++){
				Sortable.create(taskLists[i], { group: "omega" });
			}


		});


	</script>