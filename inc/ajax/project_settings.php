<?php 

require_once '../../core/init.php';

$DB = DB::getInstance();
$project = $DB->get('projects', array('id', '=', $_SESSION['project_id']))->first();

?>

<div class="form-wrapper" id='project-settings-wrapper'>
	<form method="POST" id='project-settings-form'>
	<header id="project-settings-header" style='background-image: url(<?php echo $project->feat_img; ?>)'>
		
		<?php include '../../modules/file-upload.php'; ?>

	</header>

	<div class="lower-wrapper">
		<div class="field-wrapper">
			<label for="name">Project Name</label>
			<input class='input-field' type="text" name='name' id="project-name" value="<?php echo $project->name; ?>">
		</div>

		<div class="field-wrapper">
			<label for="description">Description</label>
			<textarea class='textarea-field' name="description" id="project-description" cols="30" rows="10"><?php echo $project->description; ?></textarea>
		</div>


		<div class="field-wrapper">
			<label for="budget">Budget</label>
			<input type="number" class='input-field' name="budget" id="budget">
		</div>

		<div class="field-wrapper"></div>
		
	</div>

	<div class="field-wrapper">
		<input type="submit" value="Update">
	</div>

	</form>
	



</div>