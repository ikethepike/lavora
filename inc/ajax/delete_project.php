<?php 

	require_once('../../core/init.php');

?>

<div class="delete-wrapper pane-wrapper">
	<div class="text-wrapper">
		<header>
			<h2>Delete project</h2>	
		</header>
		<form method='POST' action="handlers/delete.php">
			<div class="field-wrapper">	

				<input type="hidden" name="id" value='<?php echo $_SESSION['project_id']; ?>'>

				<input type="hidden" name="token" value='<?php echo Token::generate(); ?>'>

				<input type="hidden" name="table" value="projects">

				<input type='hidden' name='deleteProject' value='true'>

				<div class="field-wrapper">
					<label id='delete-confirmation' for="confirmation">I understand and confirm that by clicking delete, the project and all its information will be deleted.</label>
					<input type="checkbox" required name="confirmation" id="confirmation">
				</div>


				<button>Delete Project</button>
			</div>
		</form>
		
	</div>
</div>