<?php 

require_once '../../core/init.php';

$DB = DB::getInstance();
$project = $DB->get('projects', array('id', '=', $_SESSION['project_id']))->first();

$owners = json_decode($project->owners);

$user = new User;

?>


<div class="form-wrapper pane-wrapper" id="team-management-wrapper">
	<div class="current-team">
		<h3>Current Team</h3>
		<?php foreach ($owners as $index => $owner) {
			$member = new User($owner);
			
			include '../member_orb.php'; 

		} ?>
	</div>

	<input type="hidden" name="project-owners" id="project-owners" value=''>

	<?php if(!empty($friends = json_decode($user->data()->connections))): ?>
	<div id='friends-list' class="field-wrapper">
		<h3>Connections</h3>
		<?php foreach ($friends as $key => $friend) {
			$member = new User($friend);
			include '../member_orb.php';
		} ?>
	</div>
	<?php endif; ?>

	<div class="field-wrapper email-invites">
		<input type="hidden" name="invitees" id="invitees" value=''>
		<div id="invitees-wrapper" class='invitees-inactive'>
			<header>
				<h4>People to be invited</h4>
			</header>


		</div>

		<div class="immersion-email-wrapper">
			<label for="invite-field">Invite people by email</label>
			<textarea class='email input-field invite-field' name="invite-field" id="invite-field"></textarea>
			<a id="invite-button">Invite</a>
		</div>
	</div>

</div>