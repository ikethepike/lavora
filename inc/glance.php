<?php

if(file_exists('/core')){
	require_once("core/init.php");
} else {
	require_once('../core/init.php');
}
$project_id = $_SESSION['project_id'];

$DB = DB::getInstance();

$DB->get('tasks', array('project_id', '=', $project_id));
$tasks = (array) $DB->results();


foreach($tasks as $task){
	switch ((int) $task->status) {
		case 0:
		$work['not_started'][] = $task; 
			break;
		case 1:
		$work['in_progress'][] = $task; 
			break;
		case 2:
		$work['completed'][] = $task; 
			break;
		case 3:
		$work['late'][] = $task; 
			break;
	}

}


?>

<div id="glance-view" class="<?php if(empty($work['late'])){ echo "no-late"; } ?>">
	<ul>
		<li id='listing-0' class='glance-block not-started <?php if(!isset($work['not_started'])){echo "inactive"; } ?> '>
			<header>
				Not Started
			</header>
			<ul class='task-listing' data-status='0'>

				<?php 
				if(isset($work['not_started'])):
					foreach($work['not_started'] as $task): 
						$date = date_create($task->due);
						$date = date_format($date,"M d");

						include BASE_URL . 'inc/glance_tile.php';

					endforeach; 
				endif;
				?>
			</ul>
		</li>

			<li id='listing-1' class='glance-block in-progress <?php if(!isset($work['in_progress'])){echo "inactive"; } ?> '>
				<header>
					In progress
				</header>
				<ul class='task-listing' data-status='1'>
			 		<?php 
			 			if(isset($work['in_progress'])): 
			 				foreach($work['in_progress'] as $task): 
						 		$date = date_create($task->due);
						 		$date = date_format($date,"M d");

								include BASE_URL . 'inc/glance_tile.php';


			 				 endforeach; 
						endif;
					?>
				</ul>
			</li>

		<li id='listing-2' class='glance-block completed <?php if(!isset($work['completed'])){echo "inactive"; } ?> '>
				<header>
					Completed
				</header>
				<ul class='task-listing' data-status='2'>
				<?php 
				if(isset($work['completed'])): 
					foreach($work['completed'] as $task): 
						$date = date_create($task->due);
						$date = date_format($date,"M d");

						include BASE_URL . 'inc/glance_tile.php';


					 endforeach; 
				endif; 
				?>
			</ul>
		</li>

			<li id='listing-3' class='glance-block late <?php if(!isset($work['late'])){echo "inactive"; } ?> '>
				<header>
					Late
				</header>
				<ul class='task-listing' data-status='3'>
			<?php 
				if(isset($work['late'])):
					foreach($work['late'] as $task): 
							$date = date_create($task->due);
							$date = date_format($date,"M d");
							
							include BASE_URL . 'inc/glance_tile.php';
								
					endforeach;
				endif;
			?>	
			</ul>
		</li>

	</ul>
</div>

