<div class="immersion-slide active-slide" id="immersion-slide-1">
	<div class="field-wrapper">
		<label for="project-title">Title</label>
		<input class="recap" data-link='recap-title' required type="text" name="project-title" id="project-title" value='<?php echo Input::get('project-title'); ?>'>
	</div>

	<div class="field-wrapper">	
		<label for="project-type">Project Type</label>
		<div class="suggestion-block">
			<input type="text" id="project-type" name="project-type">
			<div class="suggestions">
				<span class="suggestion" data-value='Design'>Design</span>
				<span class="suggestion" data-value='Development'>Development</span>
			</div>
		</div>
	</div>


	<div class="field-wrapper">	
		<label for="project-etc">Project completion</label>
		<input type="date" class='recap' min="<?php echo date('Y-m-d'); ?>" data-link='recap-due' name="project-etc" id="project-etc">
	</div>

	<div class="field-wrapper budget">	
		<label for="project-budget">Budget</label>
		<input type="number" class='recap' data-link='recap-budget' name="project-budget" id="project-budget" value='0'>
		<input placeholder='$' type="text" name="project-currency" id="project-currency" class='recap' data-link='recap-currency' class='currency'>
	</div>


	<div class="field-wrapper">
		<label for="project-description">Description</label>
		<textarea class="textarea" name="project-description" id="project-description" cols="10" rows="10"></textarea>
	</div>
	
		
	<div class="field-wrapper">	
		<a class="bottom-button black-button next-button">Next</a>
	</div>


</div>
<div class="immersion-slide" id="immersion-slide-2">
	<div class="field-wrapper">	
		<h2>Let's set a header</h2>
		<p>	The header will be used throughout the project</p>

	</div>

	<div class="field-wrapper file-upload-wrapper">
		<?php include 'modules/file-upload.php'; ?>
	</div>

	<div class="field-wrapper double-buttons">
		<a class="bottom-button black-button back-button">Back</a>
		<a class="bottom-button black-button next-button">Next</a>
	</div>


</div>
<div class="immersion-slide" id="immersion-slide-3">
	<div class="field-wrapper">
		<h2>Let's invite some people to join</h2>
		<p>	The more the merrier, and Do-do is much more powerful with more people</p>
	</div>

	<input type="hidden" name="project-owners" id="project-owners" value=''>

	<?php if(!empty($friends = json_decode($user->data()->connections))): ?>
	<div id='friends-list' class="field-wrapper">
		<h3>Connections</h3>
		<?php foreach ($friends as $key => $friend) {
			$member = new User($friend);
			include 'member_orb.php';
		} ?>
	</div>
	<?php endif; ?>

	<div class="field-wrapper email-invites">
		<input type="hidden" name="invitees" id="invitees" value=''>
		<div id="invitees-wrapper" class='invitees-inactive'>
			<header>
				<h4>People to be invited</h4>
			</header>


		</div>

		<div class="immersion-email-wrapper">
			<label for="invite-field">Invite people by email</label>
			<textarea class='email invite-field' name="invite-field" id="invite-field"></textarea>
			<a id="invite-button">Invite</a>
		</div>
	</div>


	<div class="field-wrapper double-buttons">
		<a class="bottom-button black-button back-button">Back</a>
		<a class="bottom-button black-button next-button">Next</a>
	</div>

</div>


<div class="immersion-slide" id="immersion-slide-4">
	<div class="field-wrapper">	
		<h2> To Recap: </h2>
	</div>

	<div class="field-wrapper stats">	
		<div class="field">
			<h4>Title</h4>
			<span id="recap-title">-</span>
		</div>

		<div class="field">	
			<h4>Budget</h4>
			<span id="recap-budget">-</span>
			<span id="recap-currency"></span>
		</div>

		<div class="field">
			<h4>Estimated Due Date</h4>
			<span id="recap-due">-</span>
			<span id="days-left"></span>
		</div>

	</div>
	<div class="field-wrapper double-buttons">
		<a class="bottom-button black-button back-button">Back</a>
		<input type='submit' id='immersion-submit-button' class="bottom-button submit-button" value='Create'>
	</div>
</div>