<?php 

if(file_exists('/core')){
	require_once("core/init.php");
} else {
	require_once('../core/init.php');
}

$DB = DB::getInstance();

$task_id = $_GET['id'];


$DB->get('tasks', array('id', '=', $task_id));

$task = $DB->first();

$DB->get('users', array('id', '=', $task->owner));

$owners = $DB->results();

?>

<div id="task-view">
	<div class='wrap'>
		<div>
			<?php if(!empty($task->feat_img)): ?>
				<header class="task-header">
					<img src="<?php echo $task->feat_img; ?>">
					<a class='image-expand' target="_blank" href="<?php echo $task->feat_img; ?>"> Click to expand </a>
				</header>
			<?php endif; ?>
			
			<header class="task-owner">
				<hr style='border-color: <?php echo $owners[0]->color; ?>'>
				<?php 
				foreach ($owners as $index => $owner) {
					echo "<div class='member' style='background: {$owner->color};'>";
					echo substr($owner->first_name, 0, 1) . substr($owner->surname, 0, 1); 
					echo "</div>";

				}

				?>

				<a role='button' class='member' id='task-add-member'> + </a>

			</header>
			
			<div id='task-text-block'>
				<h3 id="task-title"><?php echo $task->name; ?></h3>
				<span id='task-description'>
					<?php echo $task->description; ?>
				</span>
			</div>
		</div>
	</div>
</div>
		


<div id="task-comments">
	<div class="comments-wrapper">
		<header>
			<h3>Comments</h3>
		</header>
	</div>
	<div class="form-wrapper">
		<form action="" method='POST'>
			<textarea name="comment-text" id="comment-text" cols="30" rows="10"></textarea>
			<input class='submit' type="submit" value="">


		</form>
	</div>
</div>
