<?php 


$collection 	= new Collection($tasks);
$myTasks 		= $collection->search('owner', $user->data()->id);

?>

<div id="my-sidebar" role='sidebar'>
	<header class="brief">	
		<div class="slide">
			<h3>Hey, <span class="name"> <?php echo $user->data()->first_name; ?></span></h3>
		</div>
		<div class="slide">	
			<span id="your-day-text"> Here is your day </span>
		</div>

		<div class="slide">
				<span class="pre-stat">You have <?php echo count($myTasks); ?> tasks left.</span>
		</div>

		<div class="permanent date-slide">	
				<span class="date">	<?php echo date('d'); ?> </span>
				<span class="month"> <?php echo date('M'); ?></span>

		</div>
	</header>


	<div class="wrap">	
		<ul id="my-tasks">	
			<?php 

				if(!empty($myTasks)){
					foreach ($myTasks as $index => $myTask) {
						echo "<li><input type='checkbox'/>".$myTask->name."</li>";
					} 
				} ?>




		</ul>
	</div>
</div>