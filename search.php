<?php include 'inc/head.php'; ?>

</head>
<body class='search-page'>

<?php 

$search = '';
$lost 	= true;
if(isset($_GET['term'])){
	$search 	= $_GET['term'];
	$projectId 	= $_SESSION['project_id'];
	$lost 		= false;
}

?>




<?php include 'inc/header.php'; ?>

<header id="search-header">
	
	<div class="form-wrapper">
		<form method="GET">
			<input type="text" name='term' class='search-input' value="<?php echo $search; ?>">
			<input type="submit" value="Search">

		</form>
	</div>

</header>


<?php 

$DB = DB::getInstance();

$DB->search('users', $search, array('username', 'first_name', 'surname', 'email'));
$users = $DB->results();
$DB->search('tasks', $search, array('name', 'description'), array('project_id', $projectId ) );
$tasks = $DB->results();



?>


<div id="results-wrapper">
	
	<?php if(empty($tasks) && empty($users)): ?>

			<h1>Sorry, there were no results.</h1>

	<?php endif; ?>
	
	<?php if(!empty($tasks)): ?>
		<div class="results-block" id='tasks-block'>
			<?php foreach ($tasks as $index => $task) {
				include 'inc/task_result.php'; 
			} ?>
		</div>
	<?php endif; ?>

	<?php if(!empty($users)): ?>
		<div class="results-block" id='users-block'>
			<?php foreach ($users as $member) {
				$member = new User($member->id);
				include 'inc/member_orb.php'; 
			} ?>
		</div>
	<?php endif; ?>


</div>




<?php include 'inc/footer.php'; ?>
