<?php 



function friends($memberId){
	$user = new User;
	$friends = (array) json_decode($user->data()->connections);

	if(in_array($memberId, $friends)){
		return true;
	}

	return false;

}