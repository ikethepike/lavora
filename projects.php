<?php include 'inc/head.php'; ?>

</head>
<body>
	

	<?php 


		if(!$user->isLoggedIn()){
			Redirect::to(HOME_URL);
		} 

		include 'inc/userbar.php';
		include 'inc/project_view.php';
		include 'inc/task_creator.php';
		include 'inc/footer.php';
	?>