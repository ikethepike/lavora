<?php 


require_once "../core/init.php";


// Parse file
$data = file_get_contents( "php://input" ); 
$data = (array) json_decode( $data );


$hash	 	= (string) $data['hash'];
$target 	= (string) $data['table'];
$field 		= (string) $data['field'];



if(empty($hash)){
	return;
}

// Initialize db
$DB = DB::getInstance();

// Hash Match upload

$upload = $DB->get('uploads', array('hash', '=', $hash))->first()->url;


if(!$upload || empty($field)){
	return;
}



$DB->update($target, array($data['where'][0], $data['where'][1]), array($field => $upload));


if(!$DB->error()){
	die(json_encode(array('status' => true)));
}


die( $DB->error() );



return; 