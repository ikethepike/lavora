<?php 


require_once("../core/init.php");

if(!isset($_POST['token'])){
	return;
}

if(!Token::check($_POST['token'])){
	return;
}

$DB = DB::getInstance();

if(isset($_POST['id'])){

if(isset($_POST['deleteProject'] )){
	$project = $DB->get('projects', array('id', '=', $_POST['id']))->first();

	foreach (json_decode($project->owners) as $key => $owner)
	{
		// Get members involved in project and delete from their account
		$member 			= new User($owner);
		$currentProjects	= json_decode($member->data()->projects);


		// Find differences and remove current $_POST['id']
		$memberProjects 	= json_encode(array_diff($currentProjects, array($_POST['id'])));

		$DB->update('users', array('id', '=', $owner), array('projects' => $memberProjects));
	}


}

$DB->delete($_POST['table'], array('id', '=', $_POST['id']));

header(HOME_URL);

}

return false;