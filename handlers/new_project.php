<?php 


require_once "../core/init.php";

$user = new User(); 

if($user->isLoggedIn()){

	// Parse file
	$data = file_get_contents( "php://input" ); 
	$data = (array) json_decode( $data );

	if(!empty($data)){

		$contents = (array) $data['contents'];

		if($contents['feat_img'] !== null && isset($contents['feat_img'])){
			$image_hash = $contents['feat_img'];
		}

		// Initialize DB
		$DB = DB::getInstance();


		$contents['owners'] 	= json_encode(array($user->data()->id) );
		$contents['feat_img']	= $feat_img;

		// Generate project hash
		$hash = new Hash();

		$contents['hash'] = $hash->make();

		$DB->insert('projects', $contents);
		$id = $DB->id();

		$current_projects 	= json_decode($user->data()->projects);
		$current_projects[] = $id;

		// Add Project ID to user 
		$DB->update('users', $user->data()->id, array(
			'projects'	=> json_encode($current_projects),
		));

		die($id);
		
	}



	
} else {
	return false;
}