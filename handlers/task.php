<?php 


require_once "../core/init.php";


// Parse file
$data = file_get_contents( "php://input" ); 
$data = (array) json_decode( $data );

$user = new User(); 
$hash = new Hash();

if($user->isLoggedIn()){

// Initialize db
$DB = DB::getInstance();


$data['project_id'] = $_SESSION['project_id'];
$data['created_by'] = $user->data()->id;
$data['hash']		= $hash->make();
$imageHash			= $data['feat_img'];


// Hash Match
if(!empty($imageHash)){
	$DB->get('uploads', array('hash', '=', $imageHash));
	$data['feat_img'] = $DB->first()->url;
}


$DB->insert('tasks', (array) $data);

if(!$DB->error()){

	$_SESSION['task_id'] = $DB->id();
	include '../inc/flow_post.php'; 

}

}