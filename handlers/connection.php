<?php 

require_once "../core/init.php";

// Parse file
$data = file_get_contents( "php://input" ); 
$data = (array) json_decode( $data );


if(empty($data)){
	die('No data sent');
}

if(Token::check($data['token'])){
	echo false;
	return;
}


// Instantiate new user and get current connections
$user = new User;
$currentConnections = (array) json_decode($user->data()->connections);


if(empty($data['id'])){
	return;
}


$operation = 'removed';

if($currentConnections !== null && in_array($data['id'], $currentConnections)){
	$newConnections = array_diff($currentConnections, array($data['id'])); 

} else {
	$operation = 'added';
	$currentConnections[] = $data['id'];
	$newConnections = $currentConnections;
}

$DB = DB::getInstance();

$connections = json_encode($newConnections);

$DB->update('users', array('id', $user->data()->id), array(
		'connections' => $connections,
));

if(!$DB->error()){
	
	echo json_encode(array('status' => 1, 'operation' => $operation));
}



