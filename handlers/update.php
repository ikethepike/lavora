<?php 


require_once "../core/init.php";


// Parse file
$data = file_get_contents( "php://input" ); 
$data = (array) json_decode( $data );


$contents = (array) $data['contents'];


if(!empty($contents)){

// Initialize db
$DB = DB::getInstance();
$DB->update('tasks', $data['where'], $contents);

echo $DB->error();


return true;

} 
return; 