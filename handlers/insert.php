<?php 


require_once "../core/init.php";


// Parse file
$data = file_get_contents( "php://input" ); 
$data = (array) json_decode( $data );

$contents = (array) $data['contents'];


// Initialize db
$DB = DB::getInstance();
$DB->insert($data['table'], $contents);