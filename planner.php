<?php include 'inc/head.php'; ?>

</head>
<body class="planner-page">

<?php 

$new = false;

if(!isset($_GET['new'])){
	$new = true;
}

$DB = DB::getInstance();

$projectId = $_GET['proj'];

$project = $DB->get('projects', array('id', '=', $projectId))->first();











?>


<?php if($new): ?>
	<div class="page-dim page-dim-active" id='page-dim'></div>
	<div class="modal open-modal ajax-modal message-modal">
		<div class="wrapper">	
			<header>
				<h2>Let's plan <?php echo $project->name; ?> </h2>
			</header>

			<p> Your project has been created, so let's quickly get it on track and organized. This is the planner view, where you can quickly get an overview of the project and who is doing what. </p>
		</div>

		<a class="bottom-button close-button">Close</a>


	</div>
<?php endif; ?>


<?php include 'inc/planner.php'; ?>





</body>
</html>