<?php 
	require_once('core/init.php');

	$user = new User();

	if($user->isLoggedIn()):
		Redirect::to(HOME_URL);
	else:
	

	if(Input::exists() && !empty($_POST)) {
		if( Token::check(Input::get('login-token')) ) {
			$validate 	= new Validate();
			$validation = $validate->check($_POST, array(
				'login-username'	=> array('required' => true, 'item' => 'A username'),
				'login-password'	=> array('required' => true, 'item' => 'A password')
			));


			if($validation->passed()) {
				$user 		= new User();
				$remember 	= (Input::get('remember') === 'on') ? true : false;
				$login 		= $user->login(Input::get('login-username'), Input::get('login-password'), $remember); 

				if($login){
					Session::flash('login', 'login-blip');
					Redirect::to(HOME_URL . '/projects');
				} else {
					echo "<div class='site-message-wrapper errors-1'>";
					echo "<span class='error-message'>Incorrect login details</span>";
					echo "</div>";
				}

			} else {
				$number = count($validation->errors());
				echo "<div class='site-message-wrapper errors-{$number}'>";
					
					foreach($validation->errors() as $error){
						echo $error;
					}

				echo '</div>';
			}

		}
	}

	endif; // Logged in check


$subpage_title = "Login";

include 'inc/head.php'; ?>

</head>
<body>


<?php include 'inc/login_page.php'; ?>


<?php include 'inc/footer.php'; ?>