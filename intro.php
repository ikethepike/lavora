<?php 
	
	require_once('core/init.php');

	// if(!isset($_SESSION['success'])){
	// 	Redirect::to(HOME_URL); 
	// }

?>

<?php include 'inc/head.php'; ?>

</head>
<body>


<div class="intro-frame active-frame">
	<?php $user = new User(); ?>
	<div class="slide-block">
		<h1 class="intro-title">Hey there, <?php echo $user->data()->first_name; ?></h1>
	</div>
	<div class="slide-block">
		<p><?php echo $site_title; ?> is designed to help you to manage your work and to communicate progress with clients. Let's get you set up. </p>
	</div>
	
	<div class="nav-button slide-block">
		<nav>
			<button class="intro-next intro-button">Okay</button>
		</nav>
	</div>
</div>

<div class="intro-frame">
	<div class="slide-block">
		<h2>Let's give you some flair. </h2>
	</div>
	<div class="slide-block">
		<?php include 'modules/color-picker.php'; ?>
	</div>

	
	<div class="nav-button slide-block">
		<nav>
			<button class="intro-next intro-button">Next</button>
		</nav>
	</div>
</div>

<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(){

		var frames = document.getElementsByClassName('intro-frame'), 
			next   = document.getElementsByClassName('intro-next'),
			back   = document.getElementsByClassName('intro-back'),
			current = 0;

			for (var i = 0; i < next.length; i++) {
				next[i].addEventListener('click', function(){
					nextSlide();
				});
			}

			for (var i = 0; i < back.length; i++) {
				back[i].addEventListener('click', function(){
                    prevSlide();
				});
			}

			function nextSlide(){
				frames[current].className = 'intro-frame';
				current++;
				frames[current].className = 'intro-frame active-frame';
			}

			function prevSlide(){
				frames[current].className = 'intro-frame';
				current--;
				frames[current].className = 'intro-frame active-frame';
			}

	});


</script>



<?php include 'inc/footer.php'; ?>